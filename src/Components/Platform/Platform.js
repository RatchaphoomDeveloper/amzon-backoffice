import React from "react";
import Api from "../../Apis/Api";
import { useDispatch } from "react-redux";
import "./Platform.scss";
import { WEB_DOWNLOAD } from "../../env";
import MessageStaus from "../../Status/message";
const Platform = () => {
  const [version, setVersion] = React.useState("");
  const [getAllFiles, setAllFiles] = React.useState([]);
  const dispatch = useDispatch();
  const getVersion = async () => {
    try {
      const response = await Api.getVersion();
      if (response) {
        setVersion(response.version);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  const getAllfileApk = async () => {
    try {
      MessageStaus.showProgress();
      const filesArr = [];
      const response = await Api.getAllFiles();
      if (response) {
        response.filesListData[0] &&
          response.filesListData.forEach((res, i) => {
            if (res.strinG_STATUS === "ใช้งาน") {
              filesArr.push({
                fulL_LINK:res.fulL_LINK,
                version:res.version
              });
            }
          });
        MessageStaus.closeProgress();
        setAllFiles(filesArr);
      }
    } catch (error) {
      MessageStaus.errresmessage("ไม่มีไฟล์ Application")
    }
  };

  React.useEffect(() => {
    getAllfileApk();
    getVersion();
  }, []);
  return (
    <div className="platform-back-ground">
      <div className="ptt-container">
        <img src={process.env.PUBLIC_URL + "/images/ptt_digital.png"} />
      </div>
      <div className="platform-container">
        <div className="application-container">
          <img
            src={process.env.PUBLIC_URL + "/images/logo_cafe.png"}
            style={{ cursor: "pointer" }}
            onClick={() => {
              dispatch({
                type: "set",
                page: "",
              });
            }}
          />
          <h1>Install Application</h1>
        </div>
        <div
          className="android-container"
          style={{ cursor: "pointer" }}
          onClick={() => {
            window.open(getAllFiles[0] && getAllFiles[0].fulL_LINK);
          }}
        >
          <img src={process.env.PUBLIC_URL + "/images/svg_an.png"} />
          <h1>Android</h1>
        </div>
        <div className="version-container">
          <p>VERSION: {getAllFiles[0] && getAllFiles[0].version}</p>
        </div>
      </div>
    </div>
  );
};

export default Platform;
