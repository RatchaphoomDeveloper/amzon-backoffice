import React from "react";
import Api from "../../Apis/Api";
import MainContent from "../Main/MainContent";
import { useDispatch, useSelector } from "react-redux";

import "./ChangePassword.scss";
import MessageStaus from "../../Status/message";
import { INPUT_WRONG_FORMAT } from "../../Status/status";
const ChangePassword = (props) => {
  const [eyeCheck1, seteyeCheck1] = React.useState(false);
  const [eyeCheck2, seteyeCheck2] = React.useState(false);
  const [eyeCheck3, seteyeCheck3] = React.useState(false);

  const [oldPassw, setOldPassw] = React.useState("");
  const [newPassw, setNewPassw] = React.useState("");
  const [confPassw, setConfPassw] = React.useState("");
  const userData = useSelector((state) => state.user);

  const changePass = async () => {
    try {
      if (
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/.test(newPassw) ===
        false
      ) {
        return MessageStaus.errresmessage("รหัสผ่านไม่ตรงฟอร์แมต");
      }
      if (
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/.test(
          confPassw
        ) === false
      ) {
        return MessageStaus.errresmessage("รหัสผ่านไม่ตรงฟอร์แมต");
      }
      if (newPassw !== confPassw) {
        return MessageStaus.errresmessage("รหัสผ่านไม่ตรงกัน");
      }
      MessageStaus.showProgress()
      const data = {
        username: userData && userData.username,
        email: userData && userData.email,
        new_password: newPassw,
        old_password: oldPassw,
      };
      const response = await Api.changePassword(data);
      if (response.status === true) {
        MessageStaus.successmessage(
          "ระบบได้ทำการส่ง Password ใหม่ให้ทาง Email แล้ว"
        );
      }
    } catch (error) {
      MessageStaus.errresmessage("ทำรายการไม่สำเร็จ");
    }
  };

  return (
    <div>
      <div className="main-content">
        <MainContent />
        <main>
          <div className="title-container">
            <h2 className="dash-tittle">
              {/* {window.location.pathname} */}
              {props.location.pathname === "/change_password" &&
                "Managment/Change Password"}
            </h2>
            <button type="button" className="md">
              <span className="fas fa-sync"></span>
              <label>รีเฟรช</label>
            </button>
          </div>
          {/* <i class="far fa-eye"></i> */}
          {/* <i class="far fa-eye-slash"></i> */}
          <div className="forgot-password-container">
            <div className="forgot-input-form-group">
              <label>รหัสผ่านปัจจุบัน</label>
              <input
                type={eyeCheck1 === false ? "password" : "text"}
                onChange={(e) => setOldPassw(e.target.value)}
              />
              <span
                className={
                  eyeCheck1 === false ? "far fa-eye" : "far fa-eye-slash"
                }
                onClick={() => {
                  seteyeCheck1(!eyeCheck1);
                }}
              ></span>
            </div>
            <div className="forgot-input-form-group">
              <label>รหัสผ่านใหม่</label>
              <input
                type={eyeCheck2 === false ? "password" : "text"}
                onChange={(e) => setNewPassw(e.target.value)}
              />
              <span
                id={"span-eye2"}
                className={
                  eyeCheck2 === false ? "far fa-eye" : "far fa-eye-slash"
                }
                onClick={() => {
                  seteyeCheck2(!eyeCheck2);
                }}
              ></span>
            </div>
            <div className="forgot-input-form-group">
              <label>ยืนยันรหัสผ่านใหม่</label>
              <input
                type={eyeCheck3 === false ? "password" : "text"}
                onChange={(e) => setConfPassw(e.target.value)}
              />
              <span
                id={"span-eye3"}
                className={
                  eyeCheck3 === false ? "far fa-eye" : "far fa-eye-slash"
                }
                onClick={() => {
                  seteyeCheck3(!eyeCheck3);
                }}
              ></span>
            </div>
            <button type="button" onClick={changePass}>
              Confirm reset password
            </button>
          </div>
        </main>
      </div>
   
    </div>
  );
};

export default ChangePassword;
