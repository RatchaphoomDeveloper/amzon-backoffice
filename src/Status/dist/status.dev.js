"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.INPUT_LENGTH_WRONG = exports.PASSWORD_NOT_MATCH = exports.INPUT_WRONG_FORMAT = exports.INPUT_ERROR = void 0;
var INPUT_ERROR = -20;
exports.INPUT_ERROR = INPUT_ERROR;
var INPUT_WRONG_FORMAT = -21;
exports.INPUT_WRONG_FORMAT = INPUT_WRONG_FORMAT;
var PASSWORD_NOT_MATCH = -22;
exports.PASSWORD_NOT_MATCH = PASSWORD_NOT_MATCH;
var INPUT_LENGTH_WRONG = -23;
exports.INPUT_LENGTH_WRONG = INPUT_LENGTH_WRONG;