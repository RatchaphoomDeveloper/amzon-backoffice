import React from "react";
import { useSelector,useDispatch } from "react-redux";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
const MainContent = () => {
  const userData = useSelector((state) => state.user);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const dispatch = useDispatch();
  const handleLogOut = () => {
    dispatch({ type: "set_user", user: {} });
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
      <header>
        <div className="search-wrapper">
          <span className="fas fa-search"></span>
          <input type="search" placeholder="ค้นหา" />
        </div>
        <div className="social-icons">
          <span className="far fa-bell"></span>
          <span className="far fa-comment-alt"></span>
          <span className="fas fa-user-circle"   onClick={handleMenu} ></span>
          {userData.id && (
            <div>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={open}
                onClose={handleClose}
              >
                <MenuItem onClick={handleLogOut}>Log out</MenuItem>
              </Menu>
            </div>
          )}
        </div>
        
      </header>
    </div>
  );
};

export default MainContent;
