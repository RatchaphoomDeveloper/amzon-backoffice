import RouterIndex from "./Router";
import { useDispatch, useSelector } from "react-redux";
import "./App.scss";
import LoginIndex from "./Components/Login/Login";
import React from "react";
import MainMobile from "./Components/Main/MainMobile";
function App() {
  const userData = useSelector((state) => state.user);
  const dispatch = useDispatch();
  React.useEffect(() => {}, []);
  return (
    <div className="App">
      {/* {!userData.token ? <LoginIndex/> :  <RouterIndex/> } */}
     
      {!userData.token ? <LoginIndex /> : <RouterIndex />}
    </div>
  );
}

export default App;
