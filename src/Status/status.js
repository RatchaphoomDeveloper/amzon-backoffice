export const INPUT_ERROR = -20;
export const INPUT_WRONG_FORMAT = -21;
export const PASSWORD_NOT_MATCH = -22;
export const INPUT_LENGTH_WRONG = -23;
