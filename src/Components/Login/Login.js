import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Api from "../../Apis/Api";
import publicIp from "public-ip";

import MessageStaus from "../../Status/message";
import { INPUT_ERROR } from "../../Status/status";
import "../Management/mangement.scss";
import "./Login.scss";
import { useHistory } from "react-router-dom";
import Register from "../Register/Register";
import Platform from "../Platform/Platform";
import MainForgotPassword from "../MainForgotPassword/MainForgotPassword";
import Swal from "sweetalert2";
const LoginIndex = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [domain, setDomain] = useState("");
  const dispatch = useDispatch();
  const [domainList, setDomainList] = useState([]);
  const pageData = useSelector((state) => state.page);
  const getDomain = async () => {
    try {
      const response = await Api.getDomain();
      if (response) {
        setDomainList(response);
      }
    } catch (err) {
      console.error(err.message);
    }
  };
  const changePage = () => {
    dispatch({
      type: "set",
      page: "register",
    });
  };

  const backPage = () => {
    dispatch({
      type: "set",
      page: "",
    });
  };

  const handleLogin = async () => {
    try {
      const data = {
        domain: domain,
        username: username,
        password: password,
      };
      MessageStaus.showProgress()
      const response = await Api.Login(data);
      if (response.results.status === true) {
        const logData = {
          m_USER_ID: username,
          username: username,
          status: "true",
          message: "Login Passed.",
          version: "V2021.01.11",
          type: "W",
          internaL_IP: await publicIp.v4(),
          externaL_IP: await publicIp.v4(),
          iS_LOGIN: "true",
        };
        MessageStaus.closeProgress()
        await Api.saveLoginLog(logData);
        dispatch({ type: "set_user", user: response.detail });
      } else {
        const logData = {
          m_USER_ID: username,
          username: username,
          status: "false",
          message: "Login Failed.",
          version: "V2021.01.11",
          type: "W",
          internaL_IP: await publicIp.v4(),
          externaL_IP: await publicIp.v4(),
          iS_LOGIN: "false",
        };
        MessageStaus.closeProgress()
        await Api.saveLoginLog(logData);
        MessageStaus.errresmessage(response.results.message);
      }
    } catch (err) {
      console.error(err.message);
    }

    // dispatch({ type: "set_user", user: {
    //     Token : "test"
    // } })
  };
  const Validation = () => {
    try {
      if (username === "" || password === "" || domain === "") {
        return MessageStaus.messages(INPUT_ERROR);
      }
      return handleLogin();
    } catch (err) {
      console.error(err.message);
    }
  };

  const ChangePlatForm = () => {
    try {
      dispatch({
        type: "set",
        page: "platform",
      });
    } catch (err) {
      console.error(err.message);
    }
  };

  const forgotPass = () => {
    try {
      dispatch({
        type: "set",
        page: "fotgotpassword",
      });
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getDomain();
    dispatch({
      type: "set",
      page: "",
    });
  }, []);
  return (
    <div>
      {pageData === "fotgotpassword" ? (
        <div>
          <MainForgotPassword />
        </div>
      ) : (
        <div>
          {pageData === "register" ? (
            <div className="register-container">
              <div className="register-card">
                <div className="register-header-group">
                  <div className="comback-container">
                    <div>
                      <h1>คำขอใช้งานระบบ Amazon Dry Mixed</h1>
                      <p>ตรวจสอบ และ กรอกข้อมูลให้ครบถ้วน</p>
                    </div>

                    <div>
                      <button type="button" onClick={backPage}>
                        กลับหน้า Login
                      </button>
                    </div>
                  </div>
                </div>
                <div className="own-register-hr"></div>
                <div className="register-form-container">
                  <Register />
                </div>
              </div>
            </div>
          ) : (
            <div>
              {pageData === "platform" ? (
                <div className="platform-back-grounds">
                  <Platform />
                </div>
              ) : (
                <div className="login-container">
                  <div className="login-main">
                    <div className="bg-login"></div>
                    <div style={{ position: "absolute", right: "0" }}>
                      <img
                        src={process.env.PUBLIC_URL + "/images/android.png"}
                        style={{
                          width: "100px",
                          padding: "1rem",
                          cursor: "pointer",
                        }}
                        onClick={ChangePlatForm}
                      />
                    </div>

                    <div className="login-form-container">
                      <div className="login-logo-container">
                        <center>
                          <img
                            src={process.env.PUBLIC_URL + "/images/logo.png"}
                          />
                        </center>
                      </div>
                      <div className="form-select-group">
                        <select
                          name="type_com"
                          id="type_com"
                          onChange={(e) => setDomain(e.target.value)}
                        >
                          <option value="0" selected>
                            --กรุณาเลือกชนิดของผู้ใช้งาน--
                          </option>
                          {domainList &&
                            domainList.map((res, i) => {
                              return (
                                <option value={res.name}>{res.name}</option>
                              );
                            })}
                        </select>
                      </div>
                      <div className="form-login-input-group">
                        <input
                          type="text"
                          placeholder="username"
                          onChange={(e) => setUsername(e.target.value)}
                        />
                      </div>
                      <div className="form-login-input-group">
                        <input
                          type="password"
                          placeholder="password"
                          onChange={(e) => setPassword(e.target.value)}
                        />
                      </div>
                      <div className="form-login-buttom-group">
                        <button type="button" onClick={Validation}>
                          Login
                        </button>
                      </div>
                      {domain === "ไม่ผ่าน AD" && (
                        <div
                          className="forgot-password-content"
                          onClick={forgotPass}
                        >
                          <label>Forget Password</label>
                        </div>
                      )}

                      <div className="create-account-content">
                        <label>
                          {" "}
                          Don't have an account?{" "}
                          <span onClick={changePage}>Create an Account</span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default LoginIndex;
