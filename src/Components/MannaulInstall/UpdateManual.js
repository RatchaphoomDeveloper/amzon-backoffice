import React from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import Api from "../../Apis/Api";
import MessageStaus from "../../Status/message";
import MainContent from "../Main/MainContent";
import "../AppInstall/AppInstall.scss";
const UpdateManual = (props) => {
  const history = useHistory();
  const [getAllFiles, setAllFiles] = React.useState([]);
  const page_id = new URLSearchParams(props.location.search).get("FILEID");
  const [filename, setFilename] = React.useState("");
  const [version, setVersion] = React.useState("");
  const [status, setStatus] = React.useState(null);
  const updateFileStatus = async () => {
    try {
      const data = {
        id: page_id,
        status:
          status !== null
            ? status === "1"
              ? 1
              : 0
            : getAllFiles[0]["status"] === "ใช้งาน"
            ? 1
            : 0,
      };
      MessageStaus.showProgress()
      const response = await Api.updateFileStatus(data);
      if (response) {
        MessageStaus.successmessage(response.message);
        history.push("/manaul_management");
        MessageStaus.closeProgress()
      }
    } catch (error) {}
  };
  const getAllFile = async () => {
    try {
      MessageStaus.showProgress()
      const filesArr = [];
      const response = await Api.getAllManFiles();
      if (response) {
        response.filesListData[0] &&
          response.filesListData.forEach((res, i) => {
            if (res.id === page_id) {
              filesArr.push({
                ID: res.id,
                Filename: res.filename,
                Version: res.version,
                CREATE_DATE: res.creatE_DATE,
                status: res.strinG_STATUS,
                CODE_STATUS: res.codE_STATUS,
              });
            }
          });
        setAllFiles(filesArr);
        MessageStaus.closeProgress()
      }
    } catch (err) {
      console.error(err.message);
    }
  };
  React.useEffect(() => {
    getAllFile();
  }, []);
  return (
    <div>
      <div className="main-content">
        <MainContent />
        <main>
          <div className="title-container">
            <h2 className="dash-tittle">Update file status</h2>
            <div className="logic-but-content">
              <button
                type="button"
                className="md"
                id={"submit"}
                onClick={updateFileStatus}
              >
                <label>บันทึก</label>
              </button>
              <button
                type="button"
                className="md"
                onClick={() => {
                  history.push("/appinstall_management");
                }}
              >
                <label>ปิด</label>
              </button>
            </div>
          </div>
          <div className="create-file-container">
            <p className="header-files-text">Add/Edit</p>
            <div className="files-hr"></div>
            <div className="form-createfiles-container">
              <div className="form-createfile-group">
                <div id="div-1">
                  <label>
                    File Name <span>*</span>
                  </label>
                </div>
                <div>
                  <input
                    type="text"
                    disabled
                    value={getAllFiles[0] && getAllFiles[0]["Filename"]}
                    onChange={(e) => setFilename(e.target.value)}
                  />
                </div>
              </div>
              <div className="form-createfile-group">
                <div id="div-1">
                  <label>
                    Version <span>*</span>
                  </label>
                </div>
                <div>
                  <input
                    type="text"
                    disabled
                    value={getAllFiles[0] && getAllFiles[0]["Version"]}
                    onChange={(e) => setVersion(e.target.value)}
                  />
                </div>
              </div>
              <div className="form-createfile-group">
                <div id="div-1">
                  <label>
                    สถานะการใช้งาน <span>*</span>
                  </label>
                </div>
                <div>
                  <select onChange={(e) => setStatus(e.target.value)}>
                    <option selected>--กรุณาเลือก--</option>
                    {getAllFiles[0] && getAllFiles[0]["status"] === "ใช้งาน" ? (
                      <option value={1} selected>
                        ใช้งาน
                      </option>
                    ) : (
                      <option value={1}>ใช้งาน</option>
                    )}
                    {getAllFiles[0] &&
                    getAllFiles[0]["status"] === "ไม่ใช้งาน" ? (
                      <option value={0} selected>
                        ไม่ใช้งาน
                      </option>
                    ) : (
                      <option value={0}>ไม่ใช้งาน</option>
                    )}
                  </select>
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
     
    </div>
  );
};

export default UpdateManual;
