import React from "react";
import Api from "../../Apis/Api";
import MessageStaus from "../../Status/message";
import DataTableindex from "../../Utils/dataTable";
import MainContent from "../Main/MainContent";

const LogSap = () => {
  const [data, setData] = React.useState([]);
  const [q, setQ] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  const replaceHeader = [
    "ID",
    "สถานะ",
    "วันที่",
    "โดย",
    "ชื่อเมนู",
    "การทำงาน",
    "Message Return",
    "CODE_STATUS",
  ];

  function search(rows) {
    const columns = rows[0] && Object.keys(rows[0]);
    return rows.filter((row) =>
      columns.some(
        (column) =>
          row[column].toString().toLowerCase().indexOf(q.toLowerCase()) > -1
      )
    );
  }
  const getLogSaps = async () => {
    try {
      MessageStaus.showProgress()
      const dataArr = [];
      const response = await Api.getLogServices("SAP");
      if (response) {
        response.logServiceRespose.forEach((res, i) => {
          dataArr.push({
            id: res.id,
            status: res.status === 1 ? "ไม่สำเร็จ" : "สำเร็จ",
            logiN_DATE: res.actioN_DATE,
            username: res.m_USER_ID,
            API: res.m_MENU_ID,
            // MESSAGE_View: false,
            // MESSAGE_Select: false,
            MLA_CODE: res.mlA_CODE,
            JSON_RETURN: res.returN_MESSAGE,
            CODE_STATUS: res.status === 1 ? "#F14A4A" : "#5aa667",
          });
          MessageStaus.closeProgress()
        });
        setData(dataArr);
      }
    } catch (error) {}
  };

  React.useEffect(() => {
    getLogSaps();
  }, []);

  return (
    <div>
      <div className="main-content">
        <MainContent />
        <main>
          <div className="title-container">
            <h2 className="dash-tittle">Log Sap</h2>
            <button type="button" className="md">
              <span className="fas fa-sync"></span>
              <label>รีเฟรช</label>
            </button>
          </div>
          <div className="serch-container">
            <span className="fas fa-search"></span>
            <input
              type="search"
              value={q}
              onChange={(e) => setQ(e.target.value)}
            />
          </div>
          <DataTableindex data={search(data)} replaceHeader={replaceHeader} />
        </main>
      </div>
      
    </div>
  );
};

export default LogSap;
