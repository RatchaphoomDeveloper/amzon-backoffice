import React from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import Api from "../../Apis/Api";
import DataTableindex from "../../Utils/dataTable";
import MainContent from "../Main/MainContent";
import "../AppInstall/AppInstall.scss";
import MessageStaus from "../../Status/message";
const ManulInstall = (props) => {
  const history = useHistory();
  const [getAllFiles, setAllFiles] = React.useState([]);
  const [q, setQ] = React.useState("");
  const replaceHeader = [
    "Id",
    "File Name",
    "Version",
    "วันที่สร้าง",
    "สถานะ",
    "CODE_STATUS",
  ];
  function search(rows) {
    const columns = rows[0] && Object.keys(rows[0]);
    console.log(columns);
    return rows.filter((row) =>
      columns.some(
        (column) =>
          row[column].toString().toLowerCase().indexOf(q.toLowerCase()) > -1
      )
    );
  }
  const getAllFile = async () => {
    try {
      MessageStaus.showProgress()
      const filesArr = [];
      const response = await Api.getAllManFiles();
      if (response) {
        response.filesListData[0] &&
          response.filesListData.forEach((res, i) => {
            filesArr.push({
              ID: res.id,
              Filename: res.filename,
              Version: res.version,
              CREATE_DATE: res.creatE_DATE,
              status: res.strinG_STATUS,
              CODE_STATUS: res.codE_STATUS,
            });
          });
        setAllFiles(filesArr);
        MessageStaus.closeProgress()
      }else{
        MessageStaus.closeProgress()
      }
    } catch (err) {
      console.error(err.message);
      MessageStaus.closeProgress()
    }
  };

  const refreshTable = () => {
    getAllFile();
    setAllFiles([]);
  };

  React.useEffect(() => {
    getAllFile();
  }, []);
  return (
    <div>
      <div className="main-content">
        <MainContent />
        <main>
          <div className="title-container">
            <h2 className="dash-tittle">
              {/* {window.location.pathname} */}
              Manual
            </h2>
            <div className="logic-but-content">
              <button type="button" className="md" onClick={refreshTable}>
                <span className="fas fa-sync"></span>
                <label>รีเฟรช</label>
              </button>
              <button
                type="button"
                className="md"
                onClick={() => {
                  history.push("/upload_files");
                }}
              >
                <span className="fas fa-plus-circle"></span>
                <label>เพิ่ม</label>
              </button>
            </div>
          </div>
          <div>
            <DataTableindex
              data={search(getAllFiles)}
              type="edit"
              linkTo={"/appinstall_management/update_manual_status?FILEID="}
              replaceHeader={replaceHeader}
            />
          </div>
        </main>
      </div>
      
    </div>
  );
};

export default ManulInstall;
