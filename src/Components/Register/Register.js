import React from "react";
import { useHistory } from "react-router-dom";
import moment from "moment";
import "../Management/mangement.scss";
import "./Register.scss";
import Api from "../../Apis/Api";
import { useSelector, useDispatch } from "react-redux";
import MessageStaus from "../../Status/message";
import { INPUT_ERROR, INPUT_WRONG_FORMAT } from "../../Status/status";
const Register = () => {
  const history = useHistory();
  const [croleArr, setCroleArr] = React.useState([]);
  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [status, setStatus] = React.useState(null);
  const [name, setName] = React.useState("");
  const [tel, setTel] = React.useState("");
  const [position, setPosition] = React.useState("");
  const [agency, setAgency] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [wstCode, setWstCode] = React.useState(null);
  const [wstStatus, setWstStatus] = React.useState(null);
  const [domainId, setDomainId] = React.useState("");
  const [lastName, setLastName] = React.useState("");
  const [flagReg, setFlagReg] = React.useState(false);
  const [pisUser, setPisUser] = React.useState("");
  const [mUserGroupId, setMUserGroupId] = React.useState("");
  const [mPlantGroupId, setMPlantGroupId] = React.useState("");
  const [confPassword, setConfPassword] = React.useState("");
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const page_type = urlParams.get("USER_ID");
  const page_status = urlParams.get("TYPE");
  const userData = useSelector((state) => state.user);
  // const []
  const [domainList, setDomainList] = React.useState([]);
  const [userGroup, setUserGroup] = React.useState([]);
  const [plantGroup, setPlantGroup] = React.useState([]);
  const [checkStatus, setCheckStatus] = React.useState(false);
  const [considerUser, setConsiderUser] = React.useState([]);
  const dispatch = useDispatch();
  const getUserGroup = async () => {
    try {
      const response = await Api.getPermissionUserGroup();
      if (response) {
        setUserGroup(response.permissionUserDatas);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  const getPlantGroup = async () => {
    try {
      const response = await Api.getPlant();
      if (response) {
        setPlantGroup(response);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  const getDomain = async () => {
    try {
      const response = await Api.getDomain();
      if (response) {
        setDomainList(response);
      }
    } catch (err) {
      console.error(err.message);
    }
  };
  React.useEffect(() => {
    resetRegister();
  }, [croleArr]);

  React.useEffect(() => {
    getDomain();
    getUserGroup();
    getPlantGroup();
  }, []);

  const getPis = async () => {
    try {
      const pisData = {
        username: pisUser,
      };
      const response = await Api.getPis(pisData);
      if (response) {
        setUsername(response.username === null ? "" : response.username);
        const sName = response.name === null ? "" : response.name.split(" ");
        setName(sName[0]);
        setLastName(sName[1]);
        setPosition(response.position === null ? "" : response.position);
        setAgency(response.agency === null ? "" : response.agency);
        setEmail(response.email === null ? "" : response.email);
        setTel(response.tel === null ? "" : response.tel);
        setWstCode(response.wst_code === null ? "" : response.wst_code);
        setWstStatus(response.wst_status === null ? "" : response.wst_status);
      }
    } catch (err) {}
  };

  const resetRegister = () => {
    try {
      setUsername("");
      setPassword("");
      setName("");
      setAgency("");
      setLastName("");
      setStatus(null);
      setPosition("");
      setEmail("");
      setTel("");
      setWstCode(null);
      setWstStatus(null);
      setConfPassword("");
      setMPlantGroupId("");
      setMUserGroupId("");
    } catch (err) {
      console.error(err.message);
    }
  };

  const handleRegister = async () => {
    const domainFil =
      domainList[0] && domainList.filter((res, i) => res.name === domainId);
    if (
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/.test(password) ===
        false &&
      domainId === "ไม่ผ่าน AD"
    ) {
      return MessageStaus.messages(INPUT_WRONG_FORMAT);
    }
    if (
      /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(email) === false
    ) {
      return MessageStaus.errresmessage("อีเมล์ไม่ถูกฟอร์แมต");
    }
    if (
      /((\+66|0)(\d{1,2}\-?\d{3}\-?\d{3,4}))|((\+๖๖|๐)([๐-๙]{1,2}\-?[๐-๙]{3}\-?[๐-๙]{3,4}))/.test(
        tel
      ) === false
    ) {
      return MessageStaus.errresmessage("เบอร์โทร ไม่ถูกฟอร์แมต");
    }

    try {
      if (username !== "" && domainId !== "") {
        const registerData =
          domainId === "ไม่ผ่าน AD"
            ? {
                id: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                username: username,
                password: password,
                name: name + " " + lastName,
                tel: tel,
                position: position,
                agency: agency,
                email: email,
                status: status === null ? 0 : status,
                m_domain_id: domainFil[0] && domainFil[0].id,
                approve_status: 2,
                wst_code: wstCode === null ? null : wstCode,
                wst_status: wstStatus === null ? null : wstStatus,
                domain_name: domainId,
                password_expiredate: "2021-05-12T03:35:39.775Z",
              }
            : {
                id: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                username: username,
                name: name + " " + lastName,
                tel: tel,
                position: position,
                agency: agency,
                email: email,
                status: status === null ? 0 : status,
                m_domain_id: domainFil[0] && domainFil[0].id,
                wst_code: wstCode === null ? null : wstCode,
                wst_status: wstStatus === null ? null : wstStatus,
                domain_name: domainId,
                approve_status: 2,
              };
        MessageStaus.showProgress();
        const response = await Api.Register(registerData);
        if (response.results.status === true) {
          const sendEmailData = {
            template: "REG01",
            to_user_group_id:
              userGroup.filter((res, i) => res.emaiL_REGISTER === "1")[0] &&
              userGroup.filter((res, i) => res.emaiL_REGISTER === "1")[0]["id"],
            from_user_id: response.detail && response.detail.id,
            link: "",
          };
          resetRegister();
          dispatch({
            type: "set",
            page: "",
          });
          setTimeout(()=>{
            MessageStaus.successmessage("ลงทะเบียนเสร็จสิ้น")
          },500)
          await Api.sendEmail(sendEmailData);
        }
      } else {
        MessageStaus.messages(INPUT_ERROR);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  const getConsiderUserData = async () => {
    try {
      const conSiData = [];
      const response = await Api.getConsiderUser();
      if (response) {
        response &&
          response.considerUserDatas.forEach((res) => {
            conSiData.push({
              ID: res.id,
              M_DO_MAIN_NAME:
                res.m_DOMAIN_NAME === null ? "" : res.m_DOMAIN_NAME,
              USERNAME: res.username === null ? "" : res.username,
              NAME: res.name === null ? "" : res.name,
              EMAIL: res.email === null ? "" : res.email,
              status: res.approvE_STATUS === null ? "" : res.approvE_STATUS,
              CODE_STATUS:
                res.coloR_APPROVE_STATUS === null
                  ? ""
                  : res.coloR_APPROVE_STATUS,
            });
          });
        setConsiderUser(conSiData);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
    <div>
      <div className="general-container">
        <div className="general-detail" id="border">
          <div className="general-title">
            <h5>ข้อมูลทั่วไป</h5>
          </div>
          <div className="general-form">
            <div className="general-checkbox">
              <label>
                ประเภทผู้ใช้งาน
                <span>*</span>
              </label>
              <br />
              <div className="checkbox-container-inline">
                <label className="custom-checkbox">
                  <input
                    type="radio"
                    value={"PTTOR"}
                    name="domain"
                    id="PTTOR"
                    checked={domainId === "PTTOR" && true}
                    onChange={(e) => {
                      setDomainId(e.target.value);
                    }}
                  ></input>
                  <span className="checkmark"></span>
                </label>
                <label id="check-label">PTTOR</label>
                <label className="custom-checkbox">
                  <input
                    type="radio"
                    value={"BSA"}
                    name="domain"
                    id="BSA"
                    checked={domainId === "BSA" && true}
                    onChange={(e) => setDomainId(e.target.value)}
                  ></input>
                  <span className="checkmark"></span>
                </label>
                <label id="check-label">BSA</label>
                <label className="custom-checkbox">
                  <input
                    type="radio"
                    value={"PTTDIGITAL"}
                    id="PTTDIGITAL"
                    name="domain"
                    checked={domainId === "PTTDIGITAL" && true}
                    onChange={(e) => setDomainId(e.target.value)}
                  ></input>
                  <span className="checkmark"></span>
                </label>
                <label id="check-label">PTTDIGITAL</label>
                <label className="custom-checkbox">
                  <input
                    type="radio"
                    value={"ไม่ผ่าน AD"}
                    id="ไม่ผ่าน AD"
                    name="domain"
                    checked={domainId === "ไม่ผ่าน AD" && true}
                    onChange={(e) => setDomainId(e.target.value)}
                  ></input>
                  <span className="checkmark"></span>
                </label>
                <label id="check-label">ไม่ต้องการ Login ผ่าน AD</label>
              </div>
              {/* <input type="text" /> */}
            </div>
            <div className="general-input-gps">
              <label>
                รหัสพนักงาน
                <span>*</span>
              </label>
              <br />
              <input
                type="text"
                onChange={(e) => setPisUser(e.target.value)}
                disabled={domainId !== "PTTOR" && true}
              />
              <button
                type="button"
                onClick={getPis}
                disabled={domainId !== "PTTOR" && true}
              >
                ดึงข้อมูล PIS
              </button>
              <span style={{ fontSize: "12px" }}>
                {" "}
                {domainId === "PTTOR" && "กรุณากรอกรหัสพนักงาน"}{" "}
              </span>
            </div>
          </div>
          <div className="general-form">
            <div className="general-input">
              <label>ชื่อ</label>
              <br />
              <input
                type="text"
                onChange={(e) => setName(e.target.value)}
                value={name}
              />
            </div>
            <div className="general-input">
              <label>นามสกุล</label>
              <br />
              <input
                type="text"
                onChange={(e) => setLastName(e.target.value)}
                value={lastName}
              />
            </div>
          </div>
          <div className="general-form">
            <div className="general-input">
              <label>ตำแหน่ง</label>
              <br />
              <input
                type="text"
                value={position}
                onChange={(e) => setPosition(e.target.value)}
              />
            </div>
            <div className="general-input">
              <label>หน่วยงาน</label>
              <br />
              <input
                type="text"
                value={agency}
                onChange={(e) => setAgency(e.target.value)}
              />
            </div>
          </div>
          <div className="general-form">
            <div className="general-input">
              <label>อีเมล</label>
              <br />
              <input
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="general-input">
              <label>เบอร์โทร</label>
              <br />
              <input
                type="text"
                value={tel}
                onChange={(e) => setTel(e.target.value)}
              />
            </div>
          </div>
        </div>

        <div className="general-detail" id="border">
          <div className="general-title">
            <h5>ข้อมูล Login</h5>
          </div>
          <div className="general-form">
            <div className="general-checkbox">
              <label>
                Username
                <span>*</span>
              </label>
              <br />
              <input
                type="text"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </div>
          </div>
          <div className="general-form">
            {domainId !== "ไม่ผ่าน AD" ? (
              <div className="plant-form-group"></div>
            ) : (
              <div className="general-checkbox">
                <label>
                  Password
                  <span>*</span>
                </label>
                <br />
                <input
                  type="text"
                  onChange={(e) => setPassword(e.target.value)}
                />
                <br />
                <span style={{ fontSize: "14px" }}>
                  {" "}
                  {/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/.test(
                    password
                  ) === false &&
                    password !== "" &&
                    "ฟอร์แมตไม่ถูกต้อง กรุณาตั้งรหัสผ่านใหม่ (P@ssW0rd123)"}{" "}
                </span>
              </div>
            )}
            {domainId !== "ไม่ผ่าน AD" ? (
              <div className="general-checkbox"></div>
            ) : (
              <div className="general-checkbox">
                <label>
                  Confrim Password
                  <span>*</span>
                </label>
                <br />
                <input
                  type="text"
                  onChange={(e) => setConfPassword(e.target.value)}
                />
                <br />
                <span style={{ fontSize: "14px" }}>
                  {" "}
                  {/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/.test(
                    password
                  ) === false &&
                    password !== "" &&
                    "ฟอร์แมตไม่ถูกต้อง กรุณาตั้งรหัสผ่านใหม่ (P@ssW0rd123)"}{" "}
                </span>
              </div>
            )}
          </div>
        </div>

        <div className="register-button-container">
          <span>
            <input
              type="checkbox"
              onChange={(e) => setCheckStatus(e.target.checked)}
            />
            <span>ยืนยันขอสิทธิ์ใช้งาน</span>
          </span>
          <button
            type="button"
            id="register-button"
            disabled={checkStatus == false && true}
            onClick={handleRegister}
          >
            ลงทะเบียน
          </button>
        </div>
      </div>
    </div>
  );
};

export default Register;
