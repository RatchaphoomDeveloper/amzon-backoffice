import moment from "moment";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Api from "../../../../Apis/Api";
import "../../mangement.scss";
const UserLogic = ({
  props,
  setUsername,
  setPassword,
  setTel,
  setPosition,
  setAgency,
  setEmail,
  setWstCode,
  setWstStatus,
  setStatus,
  setDomainId,
  setName,
  setLastName,
  name,
  domainId,
  setFlagReg,
  setPisUser,
  lastName,
  getPis,
  position,
  agency,
  email,
  tel,
  password,
  username,
  status,
  userGroup,
  plantGroup,
  mPlantGroupId,
  setMPlantGroupId,
  setMUserGroupId,
  mUserGroupId,
  confPassword,
  setConfPassword,
  createDate,
  updateDate,
  setCreateUser,
  setUpdateUser,
  considerUserData,
  setFlagEditReg,
}) => {
  // const queryString = window.location.search;
  // const urlParams = new URLSearchParams(queryString);
  // const page_type = urlParams.get("USER_ID");
  // const page_status = urlParams.get("TYPE");
  const page_type = new URLSearchParams(props.location.search).get("USER_ID");
  const page_status = new URLSearchParams(props.location.search).get("TYPE");
  const userData = useSelector((state) => state.user);

  useEffect(() => {
    console.log(page_type)
    if (page_type !== null) {
      setFlagEditReg(true);
    } else {
      setFlagReg(true)
    }
  
    return () => {};
  }, []);

  return (
    <div>
      {/* {page_status === "approve" ? (
        <div className="general-container">
          <div className="general-detail">
            <div className="general-title">
              <h5>ข้อมูลทั่วไป</h5>
            </div>
            <div className="general-form">
              <div className="general-checkbox">
                <label>
                  ประเภทผู้ใช้งาน
                  <span>*</span>
                </label>
                <br />
                <div className="checkbox-container-inline">
                  <label className="custom-checkbox">
                    <input
                      type="radio"
                      value={"PTTOR"}
                      name="domain"
                      id="PTTOR"
                      checked={domainId === "PTTOR"}
                      onChange={(e) => {
                        setDomainId(e.target.value);
                      }}
                    ></input>
                    <span className="checkmark"></span>
                  </label>
                  <label id="check-label">PTTOR</label>
                  <label className="custom-checkbox">
                    <input
                      type="radio"
                      value={"BSA"}
                      name="domain"
                      id="BSA"
                      checked={domainId === "BSA"}
                      onChange={(e) => setDomainId(e.target.value)}
                    ></input>
                    <span className="checkmark"></span>
                  </label>
                  <label id="check-label">BSA</label>
                  <label className="custom-checkbox">
                    <input
                      type="radio"
                      value={"PTTDIGITAL"}
                      id="PTTDIGITAL"
                      name="domain"
                      checked={domainId === "PTTDIGITAL"}
                      onChange={(e) => setDomainId(e.target.value)}
                    ></input>
                    <span className="checkmark"></span>
                  </label>
                  <label id="check-label">PTTDIGITAL</label>
                  <label className="custom-checkbox">
                    <input
                      type="radio"
                      value={"ไม่ผ่าน AD"}
                      id="ไม่ผ่าน AD"
                      name="domain"
                      checked={domainId === "ไม่ผ่าน AD"}
                      onChange={(e) => setDomainId(e.target.value)}
                    ></input>
                    <span className="checkmark"></span>
                  </label>
                  <label id="check-label">ไม่ต้องการ Login ผ่าน AD</label>
                </div>
              </div>
              <div className="general-input-gps">
                <label>
                  รหัสพนักงาน
                  <span>*</span>
                </label>
                <br />
                <input
                  type="text"
                  onChange={(e) => setPisUser(e.target.value)}
                  disabled
                  disabled={domainId !== "PTTOR" && true}
                />
                <button
                  type="button"
                  onClick={getPis}
                  disabled={domainId !== "PTTOR" && true}
                >
                  ดึงข้อมูล PIS
                </button>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  {domainId === "PTTOR" &&
                    "กรุณากรอกรหัสพนักงาน หรือ ดึงข้อมูล PIS"}{" "}
                </span>
              </div>
            </div>
            <div className="general-form">
              <div className="general-input">
                <label>ชื่อ</label>
                <br />
                <input
                  type="text"
                  onChange={(e) => setName(e.target.value)}
                  disabled
                  value={name}
                />
              </div>
              <div className="general-input">
                <label>นามสกุล</label>
                <br />
                <input
                  type="text"
                  onChange={(e) => setLastName(e.target.value)}
                  disabled
                  value={lastName}
                />
              </div>
            </div>
            <div className="general-form">
              <div className="general-input">
                <label>ตำแหน่ง</label>
                <br />
                <input
                  type="text"
                  value={position}
                  disabled
                  onChange={(e) => setPosition(e.target.value)}
                />
              </div>
              <div className="general-input">
                <label>หน่วยงาน</label>
                <br />
                <input
                  type="text"
                  value={agency}
                  disabled
                  onChange={(e) => setAgency(e.target.value)}
                />
              </div>
            </div>
            <div className="general-form">
              <div className="general-input">
                <label>อีเมล</label>
                <br />
                <input
                  type="text"
                  value={email}
                  disabled
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
              <div className="general-input">
                <label>เบอร์โทร</label>
                <br />
                <input
                  type="text"
                  value={tel}
                  disabled
                  onChange={(e) => setTel(e.target.value)}
                />
              </div>
            </div>
          </div>

          <div className="general-detail">
            <div className="general-title">
              <h5>ข้อมูล Login</h5>
            </div>
            <div className="general-form">
              <div className="general-checkbox">
                <label>
                  Username
                  <span>*</span>
                </label>
                <br />
                <input
                  type="text"
                  value={username}
                  disabled
                  onChange={(e) => setUsername(e.target.value)}
                />
              </div>
              {domainId !== "ไม่ผ่าน AD" ? (
                <div className="plant-form-group">
                  <label>
                    PLant <span>*</span>
                  </label>
                  <br />

                  <select onChange={(e) => setMPlantGroupId(e.target.value)}>
                    <option selected>--กรุณาเลือก--</option>
                    {plantGroup &&
                      plantGroup.map((res, i) => {
                        return (
                          <option
                            value={res.id}
                            selected={mPlantGroupId === res.id && true}
                          >
                            {res.code + " " + res.name}
                          </option>
                        );
                      })}
                  </select>
                </div>
              ) : (
                <div className="plant-form-group">
                  <label>
                    PLant <span>*</span>
                  </label>
                  <br />
                  <select onChange={(e) => setMPlantGroupId(e.target.value)}>
                    <option selected>--กรุณาเลือก--</option>
                    {plantGroup &&
                      plantGroup.map((res, i) => {
                        return (
                          <option
                            value={res.id}
                            selected={mPlantGroupId === res.id && true}
                          >
                            {res.code + " " + res.name}
                          </option>
                        );
                      })}
                  </select>
                </div>
              )}
            </div>
            <div className="general-form">
              <div className="plant-form-group">
                <label>
                  กลุ่มผู้ใช้งาน <span>*</span>
                </label>
                <br />
                <select onChange={(e) => setMUserGroupId(e.target.value)}>
                  <option selected>--กรุณาเลือก--</option>
                  {userGroup &&
                    userGroup.map((res, i) => {
                      return (
                        res.status === "ใช้งาน" && (
                          <option
                            value={res.id}
                            selected={mUserGroupId === res.id && true}
                          >
                            {res.name}
                          </option>
                        )
                      );
                    })}
                </select>
              </div>

              <div className="general-checkbox">
                <label>
                  สถานะการใช้งาน
                  <span>*</span>
                </label>
                <br />
                <div className="checkbox-container-inline">
                  <label className="custom-checkbox">
                    <input
                      type="radio"
                      value={"1"}
                      id="1"
                      name="status"
                      checked={status === "1"}
                      onChange={(e) => setStatus(e.target.value)}
                    ></input>

                    <span className="checkmark"></span>
                  </label>
                  <label id="check-label">ใช้งาน</label>
                  <label className="custom-checkbox">
                    <input
                      type="radio"
                      value={"0"}
                      id="0"
                      name="status"
                      checked={status==="0"}
                      onChange={(e) => setStatus(e.target.value)}
                    ></input>

                    <span className="checkmark"></span>
                  </label>
                  <label id="check-label">ไม่ใช้งาน</label>
                </div>
              </div>

              {domainId === "ไม่ผ่าน AD" && (
                <div
                  className="general-checkbox"
                  style={{ marginTop: ".4rem" }}
                >
                  <label>
                    Password
                    <span>*</span>
                  </label>
                  <br />
                  <input
                    type="text"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    disabled
                  />
                  <br />
                  <span style={{ fontSize: "14px" }}>
                    {" "}
                    {/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/.test(
                      password
                    ) === false &&
                      password !== "" &&
                      "ฟอร์แมตไม่ถูกต้อง กรุณาตั้งรหัสผ่านใหม่ (P@ssW0rd123)"}{" "}
                  </span>
                </div>
              )}

              {domainId === "ไม่ผ่าน AD" && (
                <div
                  className="general-checkbox"
                  style={{ marginTop: ".6rem" }}
                >
                  <label>
                    Confrim Password
                    <span>*</span>
                  </label>
                  <br />
                  <input
                    type="text"
                    value={password}
                    onChange={(e) => setConfPassword(e.target.value)}
                    disabled
                  />
                  <br />
                  <span style={{ fontSize: "14px" }}>
                    {" "}
                    {/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/.test(
                      password
                    ) === false &&
                      password !== "" &&
                      "ฟอร์แมตไม่ถูกต้อง กรุณาตั้งรหัสผ่านใหม่ (P@ssW0rd123)"}{" "}
                  </span>
                </div>
              )}
            </div>
          </div>

          <div className="general-growth">
            <div className="general-detail">
              <div className="general-title">
                <h5>รายละเอียดผู้ทำการเพิ่มข้อมูล</h5>
              </div>
              <div className="dateTime-form-container">
                <div className="dateTime-group">
                  <label>Create on</label>
                  <input
                    type="text"
                    value={moment().format("DD/MM/YYYY hh:mm:ss").toString()}
                    disabled={true}
                  />
                </div>
                <div className="dateTime-group">
                  <label>Create By</label>
                  <input
                    type="text"
                    value={userData.username}
                    disabled={true}
                  />
                </div>
              </div>
            </div>
            <div className="general-detail">
              <div className="general-title">
                <h5>รายละเอียดผู้ทำการแก้ไขข้อมูล</h5>
              </div>
              <div className="dateTime-form-container">
                <div className="dateTime-group">
                  <label>Update on</label>
                  <input
                    type="text"
                    value={moment().format("DD/MM/YYYY hh:mm:ss").toString()}
                    disabled={true}
                  />
                </div>
                <div className="dateTime-group">
                  <label>Update By</label>
                  <input
                    type="text"
                    value={userData.username}
                    disabled={true}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        
      )} */}
      <div className="general-container">
        <div className="general-detail">
          <div className="general-title">
            <h5>ข้อมูลทั่วไป</h5>
          </div>
          <div className="general-form">
            <div className="general-checkbox">
              <label>
                ประเภทผู้ใช้งาน
                <span>*</span>
              </label>
              <br />
              <div className="checkbox-container-inline">
                <label className="custom-checkbox">
                  <input
                    type="radio"
                    value="PTTOR"
                    id="PTTOR"
                    checked={domainId === "PTTOR"}
                    onChange={(e) => setDomainId(e.target.value)}
                  ></input>

                  <span className="checkmark"></span>
                </label>
                <label id="check-label">PTTOR</label>
                <label className="custom-checkbox">
                  <input
                    type="radio"
                    value={"BSA"}
                    id="BSA"
                    checked={domainId === "BSA"}
                    onChange={(e) => setDomainId(e.target.value)}
                  ></input>

                  <span className="checkmark"></span>
                </label>
                <label id="check-label">BSA</label>
                <label className="custom-checkbox">
                  <input
                    type="radio"
                    value={"PTTDIGITAL"}
                    id="PTTDIGITAL"
                    checked={domainId === "PTTDIGITAL"}
                    onChange={(e) => setDomainId(e.target.value)}
                  ></input>

                  <span className="checkmark"></span>
                </label>
                <label id="check-label">PTTDIGITAL</label>
                <label className="custom-checkbox">
                  <input
                    type="radio"
                    value="ไม่ผ่าน AD"
                    checked={domainId === "ไม่ผ่าน AD"}
                    onChange={(e) => setDomainId(e.target.value)}
                  ></input>

                  <span className="checkmark"></span>
                </label>
                <label id="check-label">ไม่ต้องการ Login ผ่าน AD</label>
              </div>

              {/* <input type="text" /> */}
            </div>
            <div className="general-input-gps">
              <label>
                รหัสพนักงาน
                <span>*</span>
              </label>
              <br />
              <input
                type="text"
                onChange={(e) => setPisUser(e.target.value)}
                disabled={domainId !== "PTTOR" && true}
              />
              <button
                type="button"
                onClick={getPis}
                disabled={domainId !== "PTTOR" && true}
              >
                ดึงข้อมูล PIS
              </button>
              <span style={{ fontSize: "12px" }}>
                {" "}
                {domainId === "PTTOR" && "กรุณากรอกรหัสพนักงาน"}{" "}
              </span>
            </div>
          </div>
          <div className="general-form">
            <div className="general-input">
              <label>ชื่อ</label>
              <br />
              <input
                type="text"
                onChange={(e) => setName(e.target.value)}
                value={name}
              />
            </div>
            <div className="general-input">
              <label>นามสกุล</label>
              <br />
              <input
                type="text"
                onChange={(e) => setLastName(e.target.value)}
                value={lastName}
              />
            </div>
          </div>
          <div className="general-form">
            <div className="general-input">
              <label>ตำแหน่ง</label>
              <br />
              <input
                type="text"
                value={position}
                onChange={(e) => setPosition(e.target.value)}
              />
            </div>
            <div className="general-input">
              <label>หน่วยงาน</label>
              <br />
              <input
                type="text"
                value={agency}
                onChange={(e) => setAgency(e.target.value)}
              />
            </div>
          </div>
          <div className="general-form">
            <div className="general-input">
              <label>อีเมล</label>
              <br />
              <input
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="general-input">
              <label>เบอร์โทร</label>
              <br />
              <input
                type="text"
                value={tel}
                onChange={(e) => setTel(e.target.value)}
              />
            </div>
          </div>
        </div>

        <div className="general-detail">
          <div className="general-title">
            <h5>ข้อมูล Login</h5>
          </div>
          <div className="general-form">
            <div className="general-checkbox">
              <label>
                Username
                <span>*</span>
              </label>
              <br />
              <input
                type="text"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </div>
            {domainId !== "ไม่ผ่าน AD" ? (
              <div className="plant-form-group">
                <label>
                  PLant <span>*</span>
                </label>
                <br />

                <select onChange={(e) => setMPlantGroupId(e.target.value)}>
                  <option selected>--กรุณาเลือก--</option>
                  {plantGroup &&
                    plantGroup.map((res, i) => {
                      return (
                        <option
                          value={res.id}
                          selected={mPlantGroupId === res.id && true}
                        >
                          {res.code + " " + res.name}
                        </option>
                      );
                    })}
                </select>
              </div>
            ) : (
              <div className="plant-form-group">
                <label>
                  PLant <span>*</span>
                </label>
                <br />
                <select onChange={(e) => setMPlantGroupId(e.target.value)}>
                  <option selected>--กรุณาเลือก--</option>
                  {plantGroup &&
                    plantGroup.map((res, i) => {
                      return (
                        <option
                          value={res.id}
                          selected={mPlantGroupId === res.id && true}
                        >
                          {res.code + " " + res.name}
                        </option>
                      );
                    })}
                </select>
              </div>
            )}
          </div>
          <div className="general-form">
            <div className="plant-form-group">
              <label>
                กลุ่มผู้ใช้งาน <span>*</span>
              </label>
              <br />
              <select onChange={(e) => setMUserGroupId(e.target.value)}>
                <option selected>--กรุณาเลือก--</option>
                {userGroup &&
                  userGroup.map((res, i) => {
                    return (
                      res.status === "ใช้งาน" && (
                        <option
                          value={res.id}
                          selected={mUserGroupId === res.id && true}
                        >
                          {res.name}
                        </option>
                      )
                    );
                  })}
              </select>
            </div>

            <div className="general-checkbox">
              <label>
                สถานะการใช้งาน
                <span>*</span>
              </label>
              <br />

              <div className="checkbox-container-inline">
                <label className="custom-checkbox">
                  <input
                    type="radio"
                    value="1"
                    checked={status === "1"}
                    onChange={(e) => setStatus(e.target.value)}
                  ></input>

                  <span className="checkmark"></span>
                </label>
                <label id="check-label">ใช้งาน</label>
                <label className="custom-checkbox">
                  <input
                    type="radio"
                    value="0"
                    checked={status === "0"}
                    onChange={(e) => setStatus(e.target.value)}
                  ></input>
                  <span className="checkmark"></span>
                </label>
                <label id="check-label">ไม่ใช้งาน</label>
              </div>
            </div>

            {domainId === "ไม่ผ่าน AD" && (
              <div className="general-checkbox" style={{ marginTop: ".4rem" }}>
                <label>
                  Password
                  <span>*</span>
                </label>
                <br />
                <input
                  type="text"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <br />
                <span style={{ fontSize: "14px" }}>
                  {" "}
                  {/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/.test(
                    password
                  ) === false &&
                    password !== "" &&
                    "ฟอร์แมตไม่ถูกต้อง กรุณาตั้งรหัสผ่านใหม่ (P@ssW0rd123)"}{" "}
                </span>
              </div>
            )}

            {domainId === "ไม่ผ่าน AD" && (
              <div className="general-checkbox" style={{ marginTop: ".6rem" }}>
                <label>
                  Confrim Password
                  <span>*</span>
                </label>
                <br />
                <input
                  type="text"
                  value={confPassword}
                  onChange={(e) => setConfPassword(e.target.value)}
                />
                <br />
                <span style={{ fontSize: "14px" }}>
                  {" "}
                  {/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/.test(
                    password
                  ) === false &&
                    password !== "" &&
                    "ฟอร์แมตไม่ถูกต้อง กรุณาตั้งรหัสผ่านใหม่ (P@ssW0rd123)"}{" "}
                </span>
              </div>
            )}
          </div>
        </div>

        {page_type && (
          <div className="general-growth">
            <div className="general-detail">
              <div className="general-title">
                <h5>รายละเอียดผู้ทำการเพิ่มข้อมูล</h5>
              </div>
              <div className="dateTime-form-container">
                <div className="dateTime-group">
                  <label>Create on</label>
                  <input
                    type="text"
                    value={moment().format("DD/MM/YYYY hh:mm:ss").toString()}
                    disabled={true}
                  />
                </div>
                <div className="dateTime-group">
                  <label>Create By</label>
                  <input
                    type="text"
                    value={userData.username}
                    disabled={true}
                  />
                </div>
              </div>
            </div>
            <div className="general-detail">
              <div className="general-title">
                <h5>รายละเอียดผู้ทำการแก้ไขข้อมูล</h5>
              </div>
              <div className="dateTime-form-container">
                <div className="dateTime-group">
                  <label>Update on</label>
                  <input
                    type="text"
                    value={moment().format("DD/MM/YYYY hh:mm:ss").toString()}
                    disabled={true}
                  />
                </div>
                <div className="dateTime-group">
                  <label>Update By</label>
                  <input
                    type="text"
                    value={userData.username}
                    disabled={true}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default UserLogic;
