import { Component } from "react";
import Swal from "sweetalert2";
class ValidateStaus extends Component {
  static ValidationForm = (data) => {
    const columns = data && Object.keys(data);
    for (var res of columns){
        if (data[res] == "") {
            Swal.fire({
                icon: "error",
                title: "กรุณากรอกข้อมูลให้ครบทุกช่อง",
              });
            break;
           
        }else{
            return true;
        }
    }
  };
}

export default ValidateStaus;
