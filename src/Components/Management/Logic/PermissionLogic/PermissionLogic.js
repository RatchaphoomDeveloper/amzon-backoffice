import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import moment from "moment";
import Api from "../../../../Apis/Api";
import "../../mangement.scss";
import MessageStaus from "../../../../Status/message";
const PermissionLogic = ({ setCroleArr, props,setFlagPermission }) => {
  const [menusData, setMenusData] = useState([]);
  const [flagEye, setFlagEye] = useState(false);
  const [flagChkBox, setFlagChkBox] = useState(false);
  const [flagUpChkBox, setFlagUpChkBox] = useState(false);
  const [flagDelChkBox, setFlagDelChkBox] = useState(false);
  const [flagTopChkBox, setFlagTopChkBox] = useState(false);
  const [viewChekArr, setViewChekArr] = useState([]);
  const [checkCRole, setCheckCRole] = useState(0);
  const [permissionUserAllData, setPermissionUserAllData] = React.useState([]);
  const queryString = window.location.search;
  const page_type = new URLSearchParams(props.location.search).get(
    "M_MENU_GROUP_ID"
  );
  const userData = useSelector((state) => state.user);
  const getCRoles = async () => {
    try {
      MessageStaus.showProgress()
      const menuA = [];
      const data = {
        m_USER_GROUP_ID: page_type,
      };
      const response = await Api.sentCRole(data);
      if (page_type !== "") {
        if (response.cRoleResponse[0]) {
          response &&
            response.cRoleResponse.forEach((res, i) => {
              menuA.push({
                id: i + 1,
                m_menu_id: res.m_MENU_ID,
                label: res.m_MENU_NAME,
                selectedView: res.c_VIEW === "0" ? true : false,
                insSelect: res.c_ADD === "0" ? false : true,
                upSelect: res.c_EDIT === "0" ? false : true,
                delSelect: res.c_DELETE === "0" ? false : true,
                topSelect: res.c_TOP === "0" ? false : true,
                m_user_group_id: page_type,
              });
            });
          setMenusData(menuA);
          MessageStaus.closeProgress()
        } else {
          getMenus();
        }
      }
    } catch (err) {
      console.error(err.message);
      getMenus();
      MessageStaus.closeProgress()
    }
  };

  const getMenus = async () => {
    try {
      const menuA = [];
      const response = await Api.getMenus();
      if (response) {
        response &&
          response.menuResponse.forEach((res, i) => {
            console.log(res.id);
            menuA.push({
              id: i + 1,
              m_menu_id: res.id,
              label: res.name,
              selectedView: false,
              insSelect: false,
              upSelect: false,
              delSelect: false,
              topSelect: false,
              m_user_group_id: page_type,
            });
          });
        setMenusData(menuA);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  const getPermissionUserGroup = async () => {
    try {
      const response = await Api.getPermissionUserGroup();
      if (response) {
        setPermissionUserAllData(response.permissionUserDatas);
        getCRoles();
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getPermissionUserGroup();
    setFlagPermission(true)
    console.log(props);
  }, []);

  useEffect(() => {
    setCroleArr(viewChekArr);
  }, [viewChekArr]);

  return (
    <div>
   

      <div className="permission-container">
        {/* <h5>Supervisor</h5> */}
        <h5>
          {permissionUserAllData[0] &&
            permissionUserAllData.filter((res, i) => res.id === page_type)[0][
              "name"
            ]}
        </h5>
        <div className="permission-hr"></div>
        <div className="table-container fix-width">
          <table id="permission-table">
            <thead>
              <tr>
                <th rowSpan="2" style={{ width: "20%" }}>
                  เมนู
                </th>
                <th colSpan="5">Permissions</th>
              </tr>
              <tr>
                <th id="col-th">
                  <div className="th-inside">
                    <input
                      type="checkbox"
                      onChange={(e) => {
                        let checked = e.target.checked;
                        setViewChekArr(
                          menusData.map((data) => {
                            data.selectedView = checked;
                            return data;
                          })
                        );
                      }}
                    />
                    <label>View</label>
                  </div>
                </th>
                <th id="col-th">
                  <div className="th-inside">
                    <input
                      type="checkbox"
                      onChange={(e) => {
                        let checked = e.target.checked;
                        setViewChekArr(
                          menusData.map((data) => {
                            data.insSelect = checked;
                            return data;
                          })
                        );
                      }}
                    />
                    <label>Insert</label>
                  </div>
                </th>
                <th id="col-th">
                  <div className="th-inside">
                    <input
                      type="checkbox"
                      onChange={(e) => {
                        let checked = e.target.checked;
                        setViewChekArr(
                          menusData.map((data) => {
                            data.upSelect = checked;
                            return data;
                          })
                        );
                      }}
                    />
                    <label>Update</label>
                  </div>
                </th>
                <th id="col-th">
                  <div className="th-inside">
                    <input
                      type="checkbox"
                      onChange={(e) => {
                        let checked = e.target.checked;
                        setViewChekArr(
                          menusData.map((data) => {
                            data.delSelect = checked;
                            return data;
                          })
                        );
                      }}
                    />
                    <label>Delete</label>
                  </div>
                </th>
                <th id="col-th">
                  <div className="th-inside">
                    <input
                      type="checkbox"
                      onChange={(e) => {
                        let checked = e.target.checked;
                        setViewChekArr(
                          menusData.map((data) => {
                            data.topSelect = checked;
                            return data;
                          })
                        );
                      }}
                    />
                    <label>Top</label>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              {menusData &&
                menusData.map((res, i) => {
                  return (
                    <tr key={i}>
                      <td id="col-td">{res.label}</td>
                      <td style={{ textAlign: "center" }} id="eye-checkbox-id">
                        <label className="custom-eye-chekbox">
                          <input
                            type="checkbox"
                            checked={res.selectedView}
                            onChange={(e) => {
                              let checked = e.target.checked;
                              setViewChekArr(
                                menusData.map((data) => {
                                  if (res.id === data.id) {
                                    data.selectedView = checked;
                                  }
                                  return data;
                                })
                              );
                            }}
                          />
                          <span className="eye-checkmark"></span>
                        </label>
                      </td>
                      <td style={{ textAlign: "center" }}>
                        {flagChkBox === true ? (
                          <input
                            type="checkbox"
                            value={"insert" + i}
                            checked={true}
                          />
                        ) : (
                          <input
                            type="checkbox"
                            checked={res.insSelect}
                            onChange={(e) => {
                              let checked = e.target.checked;
                              setViewChekArr(
                                menusData.map((data) => {
                                  if (res.id === data.id) {
                                    data.insSelect = checked;
                                  }
                                  return data;
                                })
                              );
                            }}
                          />
                        )}
                      </td>
                      <td style={{ textAlign: "center" }}>
                        {" "}
                        {flagUpChkBox === true ? (
                          <input
                            type="checkbox"
                            value={"update" + i}
                            checked={true}
                          />
                        ) : (
                          <input
                            type="checkbox"
                            checked={res.upSelect}
                            onChange={(e) => {
                              let checked = e.target.checked;
                              setViewChekArr(
                                menusData.map((data) => {
                                  if (res.id === data.id) {
                                    data.upSelect = checked;
                                  }
                                  return data;
                                })
                              );
                            }}
                          />
                        )}
                      </td>
                      <td style={{ textAlign: "center" }}>
                        {" "}
                        {flagDelChkBox === true ? (
                          <input
                            type="checkbox"
                            value={"delete" + i}
                            checked={true}
                          />
                        ) : (
                          <input
                            type="checkbox"
                            checked={res.delSelect}
                            onChange={(e) => {
                              let checked = e.target.checked;
                              setViewChekArr(
                                menusData.map((data) => {
                                  if (res.id === data.id) {
                                    data.delSelect = checked;
                                  }
                                  return data;
                                })
                              );
                            }}
                          />
                        )}
                      </td>
                      <td style={{ textAlign: "center" }}>
                        {" "}
                        {flagTopChkBox === true ? (
                          <input
                            type="checkbox"
                            value={"top" + i}
                            checked={true}
                          />
                        ) : (
                          <input
                            type="checkbox"
                            checked={res.topSelect}
                            onChange={(e) => {
                              let checked = e.target.checked;
                              setViewChekArr(
                                menusData.map((data) => {
                                  if (res.id === data.id) {
                                    data.topSelect = checked;
                                  }
                                  return data;
                                })
                              );
                            }}
                          />
                        )}
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
        <div className="general-form">
          <div className="general-input">
            <label>Created on</label>
            <br />
            <input
              type="text"
              value={moment().format("DD/MM/YYYY hh:mm:ss").toString()}
              disabled
            />
          </div>
          <div className="general-input">
            <label>Created By</label>
            <br />
            <input type="text" value={userData.username} disabled />
          </div>
        </div>
        <div className="general-form">
          <div className="general-input">
            <label>Update on</label>
            <br />
            <input
              type="text"
              value={moment().format("DD/MM/YYYY hh:mm:ss").toString()}
              disabled
            />
          </div>
          <div className="general-input">
            <label>Update by</label>
            <br />
            <input type="text" value={userData.username} disabled />
          </div>
        </div>
      </div>
    </div>
  );
};

export default PermissionLogic;
