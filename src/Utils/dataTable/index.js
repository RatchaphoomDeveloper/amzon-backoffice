import React from "react";
import "./index.scss";
import { Link } from "react-router-dom";
import ReactPaginate from "react-paginate";
const DataTableindex = ({
  data,
  loading,
  type,
  linkTo,
  setId,
  clicked = false,
  replaceHeader,
  status,
}) => {
  const columns = data[0] && Object.keys(data[0]);
  
  const [pageNumber, setPageNumber] = React.useState(0);
  const dataPerPage = 5;
  const pagesVisited = pageNumber * dataPerPage;
  const disPlayData = data
    .slice(pagesVisited, pagesVisited + dataPerPage)
    .map((res, i) => {
      console.log(res);
      return (
        <tr key={i}>
          {columns.map((col, col_key) => {
            return (
              <td key={col_key} id={col === 'JSON_RETURN' || col === "JSON_DATA" ? "COL_SPLIT" : ""}  >
                {col.toLowerCase() !== "status" ? (
                  res[
                    col.toLowerCase() !== "status" &&
                      col !== "CODE_STATUS" &&
                      col !== "ID" &&
                      col
                  ]
                ) : (
                  <button
                    style={{ background: res.CODE_STATUS, color: "#ffff" }}
                  >
                    {" "}
                    {res.status}
                  </button>
                )}{" "}
              </td>
            );
          })}
          {type === "edit" && (
            <td>
              {clicked == true ? (
                <div
                  onClick={() => {
                    setId(res.ID);
                  }}
                >
                  <span className="fas fa-pencil-alt"></span>
                </div>
              ) : (
                <div>
                  {res.ID + "" !== "undefined" ? (
                    <div>
                      {status !== "" ? (
                        <Link to={linkTo + res.ID+"&TYPE="+status}>
                          <span className="fas fa-pencil-alt"></span>
                        </Link>
                      ) : (
                        <Link to={linkTo + res.ID}>
                          <span className="fas fa-pencil-alt"></span>
                        </Link>
                      )}
                    </div>
                  ) : (
                    <Link to={linkTo}>
                      <span className="fas fa-pencil-alt"></span>
                    </Link>
                  )}
                </div>
              )}
            </td>
          )}
        </tr>
      );
    });
  const pageCount = Math.ceil(data.length / dataPerPage);
  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };
  return (
    <div className="fix-width" >
      <table cellPadding={0} cellSpacing={0} id="dataTable">
        <thead>
          <tr>
            {!Array.isArray(replaceHeader)
              ? data[0] && columns.map((heading) => <th>{heading}</th>)
              : replaceHeader &&
                replaceHeader.map((heading) => (
                  <th>
                    {heading != "CODE_STATUS" && heading !== "Id" && heading}
                  </th>
                ))}
            {type === "edit" && <th></th>}
          </tr>
        </thead>
        <tbody>{disPlayData}</tbody>
      </table>
      <div>
        <ReactPaginate
          previousLabel={"Prev"}
          nextLabel={"Next"}
          pageCount={pageCount}
          onPageChange={changePage}
          containerClassName={"paginationBttns"}
          previousLinkClassName={"previousBttn"}
          nextLinkClassName={"nextBttn"}
          disabledClassName={"paginationDisabled"}
          activeClassName={"paginationActive"}
        />
      </div>
    </div>
  );
};

export default DataTableindex;
