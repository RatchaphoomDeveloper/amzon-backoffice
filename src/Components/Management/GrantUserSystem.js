import React from "react";
import { useHistory } from "react-router-dom";
import Api from "../../Apis/Api";
import { SERVICE_IIS_DOOR } from "../../env";
import MessageStaus from "../../Status/message";
import DataTableindex from "../../Utils/dataTable";
import MainContent from "../Main/MainContent";
import "./mangement.scss";

const GrantUserSystem = (props) => {
  const history = useHistory();
  const [q, setQ] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  const [considerUser, setConsiderUser] = React.useState([]);
  const replaceHeader = [
    "Id",
    "โดเมน",
    "ผู้ใช้งาน",
    "ชื่อ",
    "อีเมล",
    "สถานะ",
    "CODE_STATUS",
  ];

  function search(rows) {
    const columns = rows[0] && Object.keys(rows[0]);
    return rows.filter((row) =>
      columns.some(
        (column) =>
          row[column].toString().toLowerCase().indexOf(q.toLowerCase()) > -1
      )
    );
  }

  const getConsiderUserData = async () => {
    try {
      MessageStaus.showProgress()
      const conSiData = [];
      const response = await Api.getConsiderUser();
      if (response) {
        response &&
          response.considerUserDatas.forEach((res) => {
            if (res.approvE_STATUS !== "รอดำเนินการ") {
              conSiData.push({
                ID: res.id === null ? "" : res.id,
                M_DO_MAIN_NAME:
                  res.m_DOMAIN_NAME === null ? "" : res.m_DOMAIN_NAME,
                USERNAME: res.username === null ? "" : res.username,
                NAME: res.name === null ? "" : res.name,
                EMAIL: res.email === null ? "" : res.email,
                status: res.status === null ? "" : res.status,
                CODE_STATUS: res.codE_STATUS === null ? "" : res.codE_STATUS,
              });
            }
          });
        MessageStaus.closeProgress()
        setConsiderUser(conSiData);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  React.useEffect(() => {
    getConsiderUserData();
    return () => {};
  }, []);
  return (
    <div>
      <div className="main-content">
        <MainContent />
        <main>
          <div className="title-container">
            <h2 className="dash-tittle">
              {props.location.pathname ===
                SERVICE_IIS_DOOR + "grantuser_system" &&
                "Management/ผู้มีสิทธิ์ใช้งานระบบ"}
            </h2>
            <div className="logic-but-content">
              <div>
                <button
                  type="button"
                  className="md"
                  onClick={() => {
                    setConsiderUser([]);
                    getConsiderUserData();
                  }}
                >
                  <span className="fas fa-sync"></span>
                  <label>รีเฟรช</label>
                </button>
                <button
                  type="button"
                  className="md"
                  onClick={() => {
                    history.push("/consider_system/user_management");
                  }}
                >
                  <span className="fas fa-plus-circle"></span>
                  <label>เพิ่ม</label>
                </button>
              </div>
            </div>
          </div>

          <div className="serch-container">
            <span className="fas fa-search"></span>
            <input
              type="search"
              value={q}
              onChange={(e) => setQ(e.target.value)}
            />
          </div>
          <DataTableindex
            data={search(considerUser)}
            type="edit"
            linkTo={"/consider_system/user_management?USER_ID="}
            replaceHeader={replaceHeader}
          />
        </main>
      </div>
     
    </div>
  );
};

export default GrantUserSystem;
