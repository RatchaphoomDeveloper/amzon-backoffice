import React from "react";
import {
  BrowserRouter as Router,
  HashRouter,
  Switch,
  Route,
} from "react-router-dom";
import Main from "./Components/Main/Main";
import SideBar from "./Components/SideBar/SideBar";
import Footer from "./Components/Footer/Footer";
import LogLogin from "./Components/LogLogin/LogLogin";
import LogTransection from "./Components/LogTransection/LogTransection";
import ConsiderSystem from "./Components/Management/ConsiderSystem";
import ManagementLogic from "./Components/Management/Logic/ManagementLogic";
import GrantManageSystem from "./Components/Management/GrantManageSystem";
import PermissionLogic from "./Components/Management/Logic/PermissionLogic/PermissionLogic";
import GrantGroupSystem from "./Components/Management/GrantGroupSystem";
import { SERVICE_IIS_DOOR } from "./env";
import GrantUserSystem from "./Components/Management/GrantUserSystem";
import ChangePassword from "./Components/ChangePassword/ChangePassword";
import AppInstall from "./Components/AppInstall/AppInstall";
import CreateFile from "./Components/AppInstall/CreateFile";
import LogSap from "./Components/LogSap/LogSap";
import LogWebserive from "./LogWebservice/LogWebserive";
import UpdateFileStatus from "./Components/AppInstall/UpdateFileStatus";
import ManulInstall from "./Components/MannaulInstall/ManulInstall";
import UpdateManual from "./Components/MannaulInstall/UpdateManual";
import CreateManual from "./Components/MannaulInstall/CreateManual";
import MainMobile from "./Components/Main/MainMobile";
const IndexRouter = () => {
  const serviceIISDoor = "/build";
  return (
    <div>
      <HashRouter basename={SERVICE_IIS_DOOR}>
        <SideBar />
        <div className="App-normal">
          <Switch>
            <Route exact path="/" component={Main} />
            <Route exact path="/logs_login" component={LogLogin} />
            <Route exact path="/logs_transection" component={LogTransection} />
            <Route exact path="/consider_system" component={ConsiderSystem} />
            <Route exact path="/grantuser_system" component={GrantUserSystem} />
            <Route
              exact
              path="/consider_system/user_management"
              component={ManagementLogic}
            />
            <Route
              exact
              path="/grant_mange_system"
              component={GrantManageSystem}
            />
            <Route
              exact
              path="/consider_system/permission_management"
              component={ManagementLogic}
            />
            <Route
              exact
              path="/grant_user_group_system"
              component={GrantGroupSystem}
            />
            <Route exact path="/change_password" component={ChangePassword} />
            <Route exact path="/appinstall_management" component={AppInstall} />
            <Route exact path="/create_files" component={CreateFile} />
            <Route exact path="/log_saps" component={LogSap} />
            <Route exact path="/log_webservices" component={LogWebserive} />
            <Route
              exact
              path="/appinstall_management/update_file_status"
              component={UpdateFileStatus}
            />
            <Route exact path="/manaul_management" component={ManulInstall} />
            <Route
              exact
              path="/appinstall_management/update_manual_status"
              component={UpdateManual}
            />
            <Route exact path="/upload_files" component={CreateManual} />
          </Switch>
        </div>

        <div className="App-normal">
          <Footer />
        </div>
      </HashRouter>
    </div>
  );
};

export default IndexRouter;
