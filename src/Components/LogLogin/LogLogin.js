import React, { useEffect } from "react";
import Api from "../../Apis/Api";
import { SERVICE_IIS_DOOR } from "../../env";
import MessageStaus from "../../Status/message";
import DataTableindex from "../../Utils/dataTable";
import MainContent from "../Main/MainContent";
import "./LogLogin.scss";
require("es6-promise").polyfill();
require("isomorphic-fetch");
const LogLogin = (props) => {
  const [data, setData] = React.useState([]);
  const [q, setQ] = React.useState("");
  const [loading, setLoading] = React.useState(false);

  const replaceHeader = [
    "ID",
    "สถานะ",
    "วันที่",
    "โดย",
    "IP Address",
    "CODE_STATUS",
  ];

  function search(rows) {
    const columns = rows[0] && Object.keys(rows[0]);
    return rows.filter((row) =>
      columns.some(
        (column) =>
          row[column].toString().toLowerCase().indexOf(q.toLowerCase()) > -1
      )
    );
  }
  const fetchPosts = async () => {
    try {
      MessageStaus.showProgress()
      const data = [];
      const response = await Api.getLogLogin();
      if (response) {
        response &&
          response.logLoginData.forEach((res, i) => {
            data.push({
              id: res.id,
              status: res.status,
              logiN_DATE: res.logiN_DATE,
              username: res.username,
              internaL_IP: res.internaL_IP,
              CODE_STATUS: res.codE_STATUS,
            });
          });
          MessageStaus.closeProgress()
      }
      setData(data);
    } catch (err) {}
  };
  useEffect(() => {
    fetchPosts();
    setLoading(false);
    return () => {};
  }, []);
  return (
    <div>
      <div className="main-content">
        <MainContent />
        <main>
          <div className="title-container">
            <h2 className="dash-tittle">
              {/* {window.location.pathname} */}
              {props.location.pathname === "/logs_login" && "Login Logs"}
            </h2>
            <button
              type="button"
              className="md"
              onClick={() => {
                setData([]);
                fetchPosts();
              }}
            >
              <span className="fas fa-sync"></span>
              <label>รีเฟรช</label>
            </button>
          </div>

          <div className="serch-container">
            <span className="fas fa-search"></span>
            <input
              type="search"
              value={q}
              onChange={(e) => setQ(e.target.value)}
            />
          </div>
          <DataTableindex data={search(data)} replaceHeader={replaceHeader} />
        </main>
      </div>
    
    </div>
  );
};

export default LogLogin;
