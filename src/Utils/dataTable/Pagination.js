import React from "react";

const Pagination = ({ dataPerPage, totalDatas,paginate }) => {
  const pageNumber = [];
  for (let i = 1; i <= Math.ceil(totalDatas / dataPerPage); i++) {
    pageNumber.push(i);
  }
  return (
    <div>
      <nav>
        <ul>
          {pageNumber.map((number, i) => {
            return (
              <li key={i}>
                <span href="!#" onClick={()=>paginate(number)} >{number}</span>
              </li>
            );
          })}
        </ul>
      </nav>
    </div>
  );
};

export default Pagination;
