import { Component } from "react";
import Swal from "sweetalert2";
import {
  INPUT_ERROR,
  INPUT_LENGTH_WRONG,
  INPUT_WRONG_FORMAT,
  PASSWORD_NOT_MATCH,
} from "./status";

class MessageStaus extends Component {
  static messages = (data) => {
    if (data === INPUT_ERROR)
      return Swal.fire({
        icon: "error",
        title: "กรุณากรอกข้อมูลให้ครบทุกช่อง",
      });
    if (data === INPUT_WRONG_FORMAT)
      return Swal.fire({
        icon: "error",
        title: "กรุณากรอกข้อมูลให้ถูกฟอร์แมต",
      });
    if (data === PASSWORD_NOT_MATCH)
      return Swal.fire({
        icon: "error",
        title: "กรุณาตรวจสอบรหัสผ่านว่าเป็นช่องว่าง และ ตรงกันหรือไม่",
      });
    if (data === INPUT_LENGTH_WRONG)
      return Swal.fire({
        icon: "error",
        title: "จำนวนตัวอักษรเกิน",
      });
  };

  static errresmessage = (data) => {
    return Swal.fire({
      icon: "error",
      title: data,
    });
  };

  static successmessage = (data) => {
    return Swal.fire({
      icon: "success",
      title: data,
    });
  };

  static showProgress = () => {
    return Swal.fire({
      allowOutsideClick: false,
      showCancelButton: false,
      showConfirmButton: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
  };

  static closeProgress = () => {
    return Swal.close();
  };
}

export default MessageStaus;
