import React from "react";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import AndroidIcon from "@material-ui/icons/Android";
import InsertDriveFileIcon from "@material-ui/icons/InsertDriveFile";
import SettingsIcon from "@material-ui/icons/Settings";
import CodeIcon from "@material-ui/icons/Code";
import LockIcon from "@material-ui/icons/Lock";
import { useDispatch } from "react-redux";

import {
  HashRouter,
  Switch,
  NavLink,
  Route,
  useHistory,
} from "react-router-dom";
import Main from "./Main";
import LogLogin from "../LogLogin/LogLogin";
import LogTransection from "../LogTransection/LogTransection";
import ConsiderSystem from "../Management/ConsiderSystem";
import GrantUserSystem from "../Management/GrantUserSystem";
import ManagementLogic from "../Management/Logic/ManagementLogic";
import GrantManageSystem from "../Management/GrantManageSystem";
import GrantGroupSystem from "../Management/GrantGroupSystem";
import ChangePassword from "../ChangePassword/ChangePassword";
import AppInstall from "../AppInstall/AppInstall";
import CreateFile from "../AppInstall/CreateFile";
import LogSap from "../LogSap/LogSap";
import LogWebserive from "../../LogWebservice/LogWebserive";
import UpdateFileStatus from "../AppInstall/UpdateFileStatus";
import ManulInstall from "../MannaulInstall/ManulInstall";
import UpdateManual from "../MannaulInstall/UpdateManual";
import CreateManual from "../MannaulInstall/CreateManual";
import HomeIcon from "@material-ui/icons/Home";
import { SERVICE_IIS_DOOR } from "../../env";
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));

export default function MainMobile() {
  const history = useHistory();
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();
  const handleLogOut = () => {
    dispatch({ type: "set_user", user: {} });
  };
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <HashRouter basename={SERVICE_IIS_DOOR}>
        <div className={classes.root}>
          <CssBaseline />
          <AppBar
            position="fixed"
            className={clsx(classes.appBar, {
              [classes.appBarShift]: open,
            })}
          >
            <Toolbar>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                className={clsx(classes.menuButton, open && classes.hide)}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" noWrap>
                Amazon Drymixed
              </Typography>
            </Toolbar>
          </AppBar>
          <Drawer
            className={classes.drawer}
            variant="persistent"
            anchor="left"
            open={open}
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            <div className={classes.drawerHeader}>
              <IconButton onClick={handleDrawerClose}>
                {theme.direction === "ltr" ? (
                  <ChevronLeftIcon />
                ) : (
                  <ChevronRightIcon />
                )}
              </IconButton>
            </div>
            <Divider />
            <List>
              {[{ name: "Home", route: "/" }].map((text, index) => (
                <ListItem
                  button
                  key={text.name}
                  onClick={() => {
                    history.push(text.route);
                  }}
                >
                  <ListItemIcon>
                    {index % 2 === 0 ? <HomeIcon /> : <HomeIcon />}
                  </ListItemIcon>
                  <ListItemText primary={text.name} />
                </ListItem>
              ))}
            </List>
            <Divider />
            <List>
              {[
                { name: "Log Login", route: "/logs_login" },
                { name: "Log sap", route: "/log_saps" },
                { name: "Log webservice", route: "/log_webservices" },
              ].map((text, index) => (
                <ListItem
                  button
                  key={text.name}
                  onClick={() => {
                    history.push(text.route);
                  }}
                >
                  <ListItemIcon>
                    {index % 2 === 0 ? <CodeIcon /> : <CodeIcon />}
                  </ListItemIcon>
                  <ListItemText primary={text.name} />
                </ListItem>
              ))}
            </List>
            <Divider />
            <List>
              {[
                {
                  name: "พิจารณาผู้มีสิทธิใช้งานระบบ",
                  route: "/consider_system",
                },
                {
                  name: "ผู้มีสิทธิใช้งานระบบ",
                  route: "/grantuser_system",
                },
                {
                  name: "กำหนดสิทธิ์การใช้งาน",
                  route: "/grant_mange_system",
                },
                {
                  name: "กำหนดกลุ่มผู้ใช้งาน",
                  route: "/grant_user_group_system",
                },
                {
                  name: "Change Password",
                  route: "/change_password",
                },
              ].map((text, index) => (
                <ListItem
                  button
                  key={text.name}
                  onClick={() => {
                    history.push(text.route);
                  }}
                >
                  <ListItemIcon>
                    {index % 2 === 0 ? <SettingsIcon /> : <SettingsIcon />}
                  </ListItemIcon>
                  <ListItemText primary={text.name} />
                </ListItem>
              ))}
            </List>
            <Divider />
            <List>
              {[
                { name: "App Installer", route: "/appinstall_management" },
                { name: "Upload Manual", route: "/manaul_management" },
              ].map((text, index) => (
                <ListItem button  key={text.name}
                onClick={() => {
                  history.push(text.route);
                }}>
                  <ListItemIcon>
                    {index % 2 === 0 ? (
                      <AndroidIcon />
                    ) : (
                      <InsertDriveFileIcon />
                    )}
                  </ListItemIcon>
                  <ListItemText primary={text.name} />
                </ListItem>
              ))}
            </List>
            <Divider />
            <List>
              {["Log out"].map((text, index) => (
                <ListItem button key={text} onClick={handleLogOut}>
                  <ListItemIcon>
                    {index % 2 === 0 ? <LockIcon /> : <LockIcon />}
                  </ListItemIcon>
                  <ListItemText primary={text} />
                </ListItem>
              ))}
            </List>
          </Drawer>
          <main
            className={clsx(classes.content, {
              [classes.contentShift]: open,
            })}
          >
            <div className={classes.drawerHeader} />
            <Switch>
              <Route exact path="/" component={Main} />
              <Route exact path="/logs_login" component={LogLogin} />
              <Route
                exact
                path="/logs_transection"
                component={LogTransection}
              />
              <Route exact path="/consider_system" component={ConsiderSystem} />
              <Route
                exact
                path="/grantuser_system"
                component={GrantUserSystem}
              />
              <Route
                exact
                path="/consider_system/user_management"
                component={ManagementLogic}
              />
              <Route
                exact
                path="/grant_mange_system"
                component={GrantManageSystem}
              />
              <Route
                exact
                path="/consider_system/permission_management"
                component={ManagementLogic}
              />
              <Route
                exact
                path="/grant_user_group_system"
                component={GrantGroupSystem}
              />
              <Route exact path="/change_password" component={ChangePassword} />
              <Route
                exact
                path="/appinstall_management"
                component={AppInstall}
              />
              <Route exact path="/create_files" component={CreateFile} />
              <Route exact path="/log_saps" component={LogSap} />
              <Route exact path="/log_webservices" component={LogWebserive} />
              <Route
                exact
                path="/appinstall_management/update_file_status"
                component={UpdateFileStatus}
              />
              <Route exact path="/manaul_management" component={ManulInstall} />
              <Route
                exact
                path="/appinstall_management/update_manual_status"
                component={UpdateManual}
              />
              <Route exact path="/upload_files" component={CreateManual} />
            </Switch>
            {/* <Typography paragraph>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
          ut labore et dolore magna aliqua. Rhoncus dolor purus non enim praesent elementum
          facilisis leo vel. Risus at ultrices mi tempus imperdiet. Semper risus in hendrerit
          gravida rutrum quisque non tellus. Convallis convallis tellus id interdum velit laoreet id
          donec ultrices. Odio morbi quis commodo odio aenean sed adipiscing. Amet nisl suscipit
          adipiscing bibendum est ultricies integer quis. Cursus euismod quis viverra nibh cras.
          Metus vulputate eu scelerisque felis imperdiet proin fermentum leo. Mauris commodo quis
          imperdiet massa tincidunt. Cras tincidunt lobortis feugiat vivamus at augue. At augue eget
          arcu dictum varius duis at consectetur lorem. Velit sed ullamcorper morbi tincidunt. Lorem
          donec massa sapien faucibus et molestie ac.
        </Typography>
        */}
          </main>
        </div>
      </HashRouter>
    </div>
  );
}
