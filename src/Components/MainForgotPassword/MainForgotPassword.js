import React from "react";
import "./MainForgotPassword.scss";
import { useDispatch, useSelector } from "react-redux";
import { INPUT_WRONG_FORMAT } from "../../Status/status";
import MessageStaus from "../../Status/message";
import Api from "../../Apis/Api";
const MainForgotPassword = () => {
  const dispatch = useDispatch();
  const pageData = useSelector((state) => state.page);
  const [username, setUsername] = React.useState("");
  const [email, setEmail] = React.useState("");

  React.useEffect(() => {}, []);

  const userData = useSelector((state) => state.user);

  const changePass = async () => {
    try {
      if (username === "" || email === "") {
        return MessageStaus.messages(INPUT_WRONG_FORMAT);
      }
      MessageStaus.showProgress()
      const data = {
        username: username.trim(),
        email: email.trim(),
      };
      const response = await Api.forgotPassword(data);
      if (response) {
        MessageStaus.successmessage("ระบบได้ทำการส่ง Password ใหม่ให้ทาง Email แล้ว");
        dispatch({
          type: "set",
          page: "",
        });
      }
    } catch (error) {
      MessageStaus.errresmessage("ทำรายการไม่สำเร็จ");
    }
  };

  return (
    <div>
      <div className="back-home" onClick={()=>{
           dispatch({
            type: "set",
            page: "",
          });
      }} >
        <span className="fas fa-home"></span>
        <br></br>
        <label>กลับหน้า Login</label>
      </div>
      <div className="main-forgot-password-container">
        <img
          src={process.env.PUBLIC_URL + "/images/logo_cafe.png"}
          id={"forgot-logo"}
        />
        <div className="main-forgot-content">
          <h1>Forgot Password</h1>
          <p>กรุณาระบุชื่อผู้ใช้งานและอีเมลที่ใช้ในการลงทะเบียน</p>
          <div className="form-forgot-group">
            <span>Username</span>
            <input type="text" onChange={(e) => setUsername(e.target.value)} />
          </div>
          <div className="form-forgot-group">
            <span>Email</span>
            <input type="text" onChange={(e) => setEmail(e.target.value)} />
          </div>
          <button type="button" onClick={changePass}>
            Confirm to reset pasword
          </button>
        </div>
      </div>
    </div>
  );
};

export default MainForgotPassword;
