"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SERVICE_IIS_DOOR = exports.PUBLIC_URL = exports.WEB_DOWNLOAD = exports.WEB_API = void 0;
// export const  WEB_API = "https://localhost:44303/api/"
var WEB_API = "https://amzdmx-dev.pttor.com/mobile_service/api/";
exports.WEB_API = WEB_API;
var WEB_DOWNLOAD = "https://amzdmx-dev.pttor.com/mobile_service/download?file=11_05_2021_app-release.apk"; // Dev

exports.WEB_DOWNLOAD = WEB_DOWNLOAD;
var PUBLIC_URL = "https://amzdmx-dev.pttor.com/mobile_service"; // export const PUBLIC_URL="https://amzdmx-test.pttor.com/api/"
// export const PUBLIC_URL="https://amzdmx-prd.pttor.com/api/"
// Dev

exports.PUBLIC_URL = PUBLIC_URL;
var SERVICE_IIS_DOOR = "/"; // export const SERVICE_IIS_DOOR="https://amzdmx-dev.pttor.com/web_application/"
// Local
// export const SERVICE_IIS_DOOR=""

exports.SERVICE_IIS_DOOR = SERVICE_IIS_DOOR;