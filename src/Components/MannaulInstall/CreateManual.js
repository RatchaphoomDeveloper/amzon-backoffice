import React from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import Api from "../../Apis/Api";
import MessageStaus from "../../Status/message";
import MainContent from "../Main/MainContent";
import "../AppInstall/AppInstall.scss";
import { PUBLIC_URL } from "../../env";
const CreateManual = () => {
  const history = useHistory();
  const [filename, setFilename] = React.useState("");
  const [version, setVersion] = React.useState("");
  const [status, setStatus] = React.useState(null);
  const [files, setFiles] = React.useState(null);

  const SaveFile = async () => {
    try {
      MessageStaus.showProgress()
      let formdata = new FormData();
      formdata.append("files", files);
      const response = await Api.saveApkFile(
        formdata,
        filename,
        version,
        parseInt(status),
        PUBLIC_URL,
        "MNL"
      );
      if (response) {
        MessageStaus.successmessage(response.message);
        history.push("/manaul_management");
        MessageStaus.closeProgress()
      }
    } catch (error) {}
  };

  return (
    <div>
      <div className="main-content">
        <MainContent />
        <main>
          <div className="title-container">
            <h2 className="dash-tittle">Upload Manual</h2>
            <div className="logic-but-content">
              <button
                type="button"
                className="md"
                id={"submit"}
                onClick={SaveFile}
              >
                <label>บันทึก</label>
              </button>
              <button
                type="button"
                className="md"
                onClick={() => {
                  history.push("/appinstall_management");
                }}
              >
                <label>ปิด</label>
              </button>
            </div>
          </div>
          <div className="create-file-container">
            <p className="header-files-text">Add/Edit</p>
            <div className="files-hr"></div>
            <div className="form-createfiles-container">
              <div className="form-createfile-group">
                <div id="div-1">
                  <label>
                    File Name <span>*</span>
                  </label>
                </div>
                <div>
                  <input
                    type="text"
                    onChange={(e) => setFilename(e.target.value)}
                  />
                </div>
              </div>
              <div className="form-createfile-group">
                <div id="div-1">
                  <label>
                    Version <span>*</span>
                  </label>
                </div>
                <div>
                  <input
                    type="text"
                    onChange={(e) => setVersion(e.target.value)}
                  />
                </div>
              </div>
              <div className="form-createfile-group">
                <div id="div-1">
                  <label>
                    สถานะการใช้งาน <span>*</span>
                  </label>
                </div>
                <div>
                  <select onChange={(e) => setStatus(e.target.value)}>
                    <option selected>--กรุณาเลือก--</option>
                    <option value={1}>ใช้งาน</option>
                    <option value={0}>ไม่ใช้งาน</option>
                  </select>
                </div>
              </div>
              <div className="form-createfile-group">
                <div id="div-1">
                  <label>
                    Upload File <span>*</span>
                  </label>
                </div>
                <div>
                  <input
                    type="file"
                    onChange={(e) => setFiles(e.target.files[0])}
                  />
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
     
    </div>
  );
};

export default CreateManual;
