import React from "react";
import { useSelector } from "react-redux";
import MainContent from "../../Main/MainContent";
import "../mangement.scss";
import PermissionLogic from "./PermissionLogic/PermissionLogic";
import UserLogic from "./UserLogic/UserLogic";
import { useHistory } from "react-router-dom";
import { SERVICE_IIS_DOOR } from "../../../env";
import Api from "../../../Apis/Api";
import MessageStaus from "../../../Status/message";
import { INPUT_ERROR, INPUT_WRONG_FORMAT } from "../../../Status/status";
import { v4 as uuidv4 } from "uuid";

// import uuid from 'uuid'
// import nJwt  from 'njwt'
import moment from "moment";
const ManagementLogic = (props) => {
  const history = useHistory();
  const [croleArr, setCroleArr] = React.useState([]);
  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [status, setStatus] = React.useState(null);
  const [name, setName] = React.useState("");
  const [tel, setTel] = React.useState("");
  const [position, setPosition] = React.useState("");
  const [agency, setAgency] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [wstCode, setWstCode] = React.useState(null);
  const [wstStatus, setWstStatus] = React.useState(null);
  const [domainId, setDomainId] = React.useState("");
  const [lastName, setLastName] = React.useState("");
  const [flagReg, setFlagReg] = React.useState(false);
  const [flagEditReg, setFlagEditReg] = React.useState(false);
  const [flagPermission, setFlagPermission] = React.useState(false);
  const [pisUser, setPisUser] = React.useState("");
  const [mUserGroupId, setMUserGroupId] = React.useState("");
  const [mPlantGroupId, setMPlantGroupId] = React.useState("");
  const [confPassword, setConfPassword] = React.useState("");
  const [createDate, setCreateDate] = React.useState("");
  const [updateDate, setUpdateDate] = React.useState("");
  const [createUser, setCreateUser] = React.useState("");
  const [updateUser, setUpdateUser] = React.useState("");

  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  // const page_type = urlParams.get("USER_ID");
  // const page_status = urlParams.get("TYPE");
  const page_type = new URLSearchParams(props.location.search).get("USER_ID");
  const page_status = new URLSearchParams(props.location.search).get("TYPE");
  const userData = useSelector((state) => state.user);
  // const []
  const [domainList, setDomainList] = React.useState([]);
  const [userGroup, setUserGroup] = React.useState([]);
  const [plantGroup, setPlantGroup] = React.useState([]);
  const [considerUserData, SetConsiderUserData] = React.useState([]);

  const getAllUser = async () => {
    try {
      const resposne = await Api.getConsiderUser();
      if (resposne) {
        SetConsiderUserData(resposne && resposne.considerUserDatas);
      }
    } catch (error) {
      console.error(error.message);
    }
  };

  const editUser = async () => {
    const domainFil =
      domainList[0] && domainList.filter((res, i) => res.name === domainId);
    if (
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/.test(password) ===
        false &&
      domainId === "ไม่ผ่าน AD"
    ) {
      return MessageStaus.messages(INPUT_WRONG_FORMAT);
    }
    // if (password !== confPassword) {
    //   return MessageStaus.errresmessage("รหัสผ่านไม่ตรงกัน");
    // }

    if (domainId !== "ไม่ผ่าน AD" && status === null) {
      return MessageStaus.messages(INPUT_WRONG_FORMAT);
    }

    try {
      if (username !== "" && domainId !== "") {
        MessageStaus.showProgress();
        console.log(mUserGroupId)
        const registerData =
          domainId !== "ไม่ผ่าน AD"
            ? {
                id: page_type,
                username: username,
                password: password === "" ? "" : password,
                name: name + " " + lastName,
                tel: tel,
                position: position,
                agency: agency,
                email: email,
                token: "string",
                status: status === null ? "0" : status.toString(),
                m_domain_id: domainFil[0] && domainFil[0].id,
                m_USER_GROUP_ID: mUserGroupId,
                user_group_name:
                  mUserGroupId &&
                  (userGroup.filter((res, i) => res.id === mUserGroupId)[0]
                    ? userGroup.filter((res, i) => res.id === mUserGroupId)[0][
                        "code"
                      ]
                    : uuidv4()),
                m_PLANT_ID: mPlantGroupId,
                m_user_id_create: userData && userData.id,
                m_user_id_update: userData && userData.id,
                approve_status: "0",
                m_user_id_Approve: userData && userData.id,
                wst_code: wstCode === null ? null : wstCode,
                wst_status: wstStatus === null ? null : wstStatus,
                plant_code:
                  mUserGroupId &&
                  (plantGroup.filter((res, i) => res.id === mUserGroupId)[0]
                    ? plantGroup.filter((res, i) => res.id === mUserGroupId)[0][
                        "code"
                      ]
                    : uuidv4()),
                domain_name: domainId,
              }
            : {
                id: page_type,
                username: username,
                password: password === "" ? "" : password,
                name: name + " " + lastName,
                tel: tel,
                position: position,
                agency: agency,
                email: email,
                status: status === null ? "0" : status.toString(),
                m_domain_id: domainFil[0] && domainFil[0].id,
                // m_USER_GROUP_ID:
                //   domainId === "ไม่ผ่าน AD"
                //     ? "3fa85f64-5717-4562-b3fc-2c963f66afa6"
                //     : mUserGroupId,
                m_USER_GROUP_ID:mUserGroupId,
                user_group_name:
                  mUserGroupId &&
                  (userGroup.filter((res, i) => res.id === mUserGroupId)[0]
                    ? userGroup.filter((res, i) => res.id === mUserGroupId)[0][
                        "code"
                      ]
                    : uuidv4()),
                m_PLANT_ID: mPlantGroupId === "" ? null : mPlantGroupId,
                wst_code: wstCode === null ? null : wstCode,
                wst_status: wstStatus === null ? null : wstStatus,
                plant_code:
                  mUserGroupId &&
                  (plantGroup.filter((res, i) => res.id === mUserGroupId)[0]
                    ? plantGroup.filter((res, i) => res.id === mUserGroupId)[0][
                        "code"
                      ]
                    : uuidv4()),
                domain_name: domainId,
                m_user_id_create: userData && userData.id,
                m_user_id_update: userData && userData.id,
              };
        const response = await Api.editUSer(registerData);
        // const response = await Api.Register(registerData);
        if (response) {
          setTimeout(()=>{
            MessageStaus.successmessage(response.message);
          },500)
          resetRegister();
          history.push("/grantuser_system");
          // MessageStaus.closeProgress();
        }
      } else {
        MessageStaus.messages(INPUT_ERROR);
        // resetRegister();
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  const getUserGroup = async () => {
    try {
      const response = await Api.getPermissionUserGroup();
      if (response) {
        setUserGroup(response.permissionUserDatas);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  const getPlantGroup = async () => {
    try {
      const response = await Api.getPlant();
      if (response) {
        setPlantGroup(response);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  const getDomain = async () => {
    try {
      const response = await Api.getDomain();
      if (response) {
        setDomainList(response);
        getAllUser();
      }
    } catch (err) {
      console.error(err.message);
    }
  };
  React.useEffect(() => {
    resetRegister();
  }, [croleArr]);

  React.useEffect(() => {
    getDomain();
    {
      page_type !== null && getUser();
    }
    {
      page_status !== undefined && getUserGroup();
    }
    getPlantGroup();
    resetRegister();
  }, []);

  // React.useEffect(()=>{
  //   setStatus(status)
  //   console.log(status)
  // },[status])

  const getUser = async () => {
    try {
      MessageStaus.showProgress();
      const userData = {
        userID: page_type !== null && page_type,
      };

      const response = await Api.getUser(userData);
      if (response && page_type !== null) {
        setUsername(response[0] && response[0].username);
        setPassword(
          response[0] && response[0].password === null
            ? ""
            : response[0].password
        );
        setConfPassword(
          response[0] && response[0].password === null
            ? ""
            : response[0].password
        );
        setName(response[0] && response[0].name.split(" ")[0]);
        setAgency(response[0] && response[0].agency);
        setLastName(response[0] && response[0].name.split(" ")[1]);
        setStatus(response[0] && response[0].status.toString());
        setPosition(response[0] && response[0].position);
        setEmail(response[0] && response[0].email);
        setTel(response[0] && response[0].tel);
        setWstCode(response[0] && response[0].wst_code);
        setWstStatus(response[0] && response[0].wst_status);
        setDomainId(response[0] && response[0].domain_name.toString());
        setMUserGroupId(response[0] && response[0].m_user_group_id);
        setMPlantGroupId(response[0] && response[0].m_plant_id);
        MessageStaus.closeProgress();
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  const getPis = async () => {
    try {
      MessageStaus.showProgress();
      const pisData = {
        username: pisUser,
      };
      const response = await Api.getPis(pisData);
      if (response) {
        setUsername(response.username === null ? "" : response.username);
        const sName = response.name === null ? "" : response.name.split(" ");
        setName(sName[0]);
        setLastName(sName[1]);
        setPosition(response.position === null ? "" : response.position);
        setAgency(response.agency === null ? "" : response.agency);
        setEmail(response.email === null ? "" : response.email);
        setTel(response.tel === null ? "" : response.tel);
        setWstCode(response.wst_code === null ? "" : response.wst_code);
        setWstStatus(response.wst_status === null ? "" : response.wst_status);
        MessageStaus.closeProgress();
      }
    } catch (err) {}
  };
  const handleSubmit = async () => {
    try {
      if (croleArr.length !== 0) {
        let cRoleArrs = [];
        croleArr[0] &&
          croleArr.forEach((res, i) => {
            cRoleArrs.push({
              id: i + 1,
              m_menu_id: res.m_menu_id,
              label: res.label,
              selectedView: res.selectedView === false ? true : false,
              insSelect: res.insSelect === false ? false : true,
              upSelect: res.upSelect === false ? false : true,
              delSelect: res.delSelect === false ? false : true,
              topSelect: res.topSelect === false ? false : true,
              m_user_group_id: res.m_user_group_id,
            });
          });
        MessageStaus.showProgress();
        const response = await Api.saveCRole({
          crolE_JSON_STR: JSON.stringify(cRoleArrs),
        });
        if (response) {
          setTimeout(()=>{
            MessageStaus.successmessage(response.message);
          },500)
          history.push("/grant_mange_system");
          setCroleArr([]);
          // MessageStaus.closeProgress();
        }
      } else {
        MessageStaus.errresmessage("ไม่มีข้อมูลเปลี่ยนแปลง");
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  const changeStatus = async () => {
    try {
      MessageStaus.showProgress();
      const userStatus = {
        userid: page_type,
        status: status,
        userupdateid: userData.id,
      };
      const response = await Api.changeStatus(userStatus);
      if (response.status === true) {
        setTimeout(()=>{
          MessageStaus.successmessage(response.message);
        },500)
        history.push("/grantuser_system");
        // MessageStaus.closeProgress();
      } else {
        MessageStaus.errresmessage("ทำรายการไม่สำเร็จ");
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  const resetRegister = () => {
    try {
      setUsername("");
      setPassword("");
      setName("");
      setAgency("");
      setLastName("");
      setStatus(null);
      setPosition("");
      setEmail("");
      setTel("");
      setWstCode(null);
      setWstStatus(null);
      setConfPassword("");
      setMPlantGroupId("");
      setMUserGroupId("");
    } catch (err) {
      console.error(err.message);
    }
  };

  const handleRegister = async () => {
    const domainFil =
      domainList[0] && domainList.filter((res, i) => res.name === domainId);
    if (
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/.test(password) ===
        false &&
      domainId === "ไม่ผ่าน AD"
    ) {
      return MessageStaus.messages(INPUT_WRONG_FORMAT);
    }
    if (
      /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(email) === false
    ) {
      return MessageStaus.errresmessage("อีเมล์ไม่ถูกฟอร์แมต");
    }
    if (
      /((\+66|0)(\d{1,2}\-?\d{3}\-?\d{3,4}))|((\+๖๖|๐)([๐-๙]{1,2}\-?[๐-๙]{3}\-?[๐-๙]{3,4}))/.test(
        tel
      ) === false
    ) {
      return MessageStaus.errresmessage("เบอร์โทรไม่ถูกฟอร์แมต");
    }

    try {
      if (username !== "" && domainId !== "") {
        const registerData =
          domainId !== "ไม่ผ่าน AD"
            ? {
                id: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                username: username,
                name: name + " " + lastName,
                tel: tel,
                position: position,
                agency: agency,
                email: email,
                token: "string",
                status: status,
                m_domain_id: domainFil[0] && domainFil[0].id,
                m_user_group_id: mUserGroupId,
                user_group_name:
                  mUserGroupId &&
                  (userGroup.filter((res, i) => res.id === mUserGroupId)[0]
                    ? userGroup.filter((res, i) => res.id === mUserGroupId)[0][
                        "code"
                      ]
                    : uuidv4()),
                m_plant_id: mPlantGroupId,
                m_user_id_create: userData && userData.id,
                m_user_id_update: userData && userData.id,
                approve_status: 0,
                m_user_id_Approve: userData && userData.id,
                wst_code: wstCode === null ? null : wstCode,
                wst_status: wstStatus === null ? null : wstStatus,
                plant_code:
                  mUserGroupId &&
                  (plantGroup.filter((res, i) => res.id === mUserGroupId)[0]
                    ? plantGroup.filter((res, i) => res.id === mUserGroupId)[0][
                        "code"
                      ]
                    : uuidv4()),
                domain_name: domainId,
              }
            : {
                id: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                username: username,
                password: password,
                name: name + " " + lastName,
                tel: tel,
                position: position,
                agency: agency,
                email: email,
                status: status === null ? 0 : status,
                m_domain_id: domainFil[0] && domainFil[0].id,
                m_user_group_id:
                  domainId === "ไม่ผ่าน AD"
                    ? "3fa85f64-5717-4562-b3fc-2c963f66afa6"
                    : mUserGroupId,
                user_group_name:
                  mUserGroupId &&
                  (userGroup.filter((res, i) => res.id === mUserGroupId)[0]
                    ? userGroup.filter((res, i) => res.id === mUserGroupId)[0][
                        "code"
                      ]
                    : uuidv4()),
                m_plant_id: mPlantGroupId === "" ? null : mPlantGroupId,
                wst_code: wstCode === null ? null : wstCode,
                wst_status: wstStatus === null ? null : wstStatus,
                plant_code:
                  mUserGroupId &&
                  (plantGroup.filter((res, i) => res.id === mUserGroupId)[0]
                    ? plantGroup.filter((res, i) => res.id === mUserGroupId)[0][
                        "code"
                      ]
                    : uuidv4()),
                domain_name: domainId,
                m_user_id_create: userData && userData.id,
                m_user_id_update: userData && userData.id,
              };
        MessageStaus.showProgress();
        const response = await Api.Register(registerData);
        if (response) {
          const sendEmailData = {
            template: "REG01",
            to_user_group_id:
              userGroup.filter((res, i) => res.emaiL_REGISTER === "1")[0] &&
              userGroup.filter((res, i) => res.emaiL_REGISTER === "1")[0]["id"],
            from_user_id: response.detail && response.detail.id,
            link: "",
          };
          setTimeout(()=>{
            MessageStaus.successmessage(response.results.message);
          },500)
          resetRegister();
          history.push("/consider_system");
          // MessageStaus.closeProgress();
          await Api.sendEmail(sendEmailData);
       
        }
      } else {
        MessageStaus.messages(INPUT_ERROR);
        resetRegister();
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  const approveStatus = async () => {
    try {
      const appData = {
        userid: page_type,
        isapprove: true,
        userapprove: userData.id,
        usergroupid: mUserGroupId,
        plantid: mPlantGroupId,
      };
      const response = await Api.approveStatus(appData);
      if (response.status === true) {
        const sendEmailData = {
          template: "APR01",
          to_user_group_id:
            userGroup.filter((res, i) => res.emaiL_REGISTER === "1")[0] &&
            userGroup.filter((res, i) => res.emaiL_REGISTER === "1")[0]["id"],
          from_user_id: page_type,
          link: "",
        };
        MessageStaus.successmessage(response.message);
        history.push("/consider_system");
        editUser();
        await Api.sendEmail(sendEmailData);
      } else {
        MessageStaus.messages(INPUT_ERROR);
      }
    } catch (err) {
      console.error(err.message);
    }
  };
  const approveCancel = async () => {
    try {
      const appData = {
        userid: page_type,
        isapprove: false,
        userapprove: userData.id,
        usergroupid: mUserGroupId,
        plantid: mPlantGroupId,
      };
      const response = await Api.approveStatus(appData);
      if (response.status === true) {
        const sendEmailData = {
          template: "REJ01",
          to_user_group_id:
            userGroup.filter((res, i) => res.emaiL_REGISTER === "1")[0] &&
            userGroup.filter((res, i) => res.emaiL_REGISTER === "1")[0]["id"],
          from_user_id: page_type,
          link: "",
        };
        MessageStaus.showProgress();
        setTimeout(()=>{
          MessageStaus.successmessage(response.message);
        },500)
        history.push("/consider_system");
        await Api.sendEmail(sendEmailData);
      } else {
        MessageStaus.messages(INPUT_ERROR);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
    <div>
      <div className="main-content">
        <MainContent />
        <main>
          <div className="title-container">
            <h2 className="dash-tittle">
              {props.location.pathname ===
                SERVICE_IIS_DOOR + "consider_system/user_management" &&
                "Management/ผู้มีสิทธิใช้งานระบบ"}
              {props.location.pathname ===
                SERVICE_IIS_DOOR + "consider_system/permission_management" &&
                "Managment/กำหนดสิทธิ์การใช้งาน"}
            </h2>
            {props.location.pathname === "/consider_system/user_management" &&
            page_status === "approve" &&
            considerUserData.filter((res, i) => res.id === page_type)[0] &&
            considerUserData.filter((res, i) => res.id === page_type)[0][
              "approvE_STATUS"
            ] !== "อนุมัติ" ? (
              <div className="logic-but-content">
                <button
                  type="button"
                  className=""
                  id="approve"
                  onClick={approveStatus}
                >
                  อนุมัติ
                </button>
                <button
                  type="button"
                  className=""
                  id="cancel"
                  onClick={approveCancel}
                >
                  ปฏิเสธ
                </button>
                <button
                  type="button"
                  className=""
                  id="close"
                  onClick={() => {
                    history.goBack()
                  }}
                >
                  ปิด
                </button>
              </div>
            ) : (
              <div className="logic-but-content">
                {flagEditReg === true ? (
                  <button
                    type="button"
                    className="md"
                    id="button-submit"
                    onClick={editUser}
                  >
                    <label>บันทึก</label>
                  </button>
                ) : (
                  <button
                    type="button"
                    className="md"
                    id="button-submit"
                    onClick={
                      flagPermission === true 
                        ? handleSubmit
                        : page_type === null
                        ? handleRegister
                        : changeStatus
                    }
                  >
                    <label>บันทึก</label>
                  </button>
                )}

                <button
                  type="button"
                  className="md"
                  onClick={() => {
                    history.goBack()
                  }}
                >
                  <label>ปิด</label>
                </button>
              </div>
            )}
          </div>
          {props.location.pathname ===
            SERVICE_IIS_DOOR + "consider_system/user_management" && (
            <UserLogic
              setUsername={setUsername}
              setPassword={setPassword}
              setName={setName}
              setTel={setTel}
              setPosition={setPosition}
              setAgency={setAgency}
              setEmail={setEmail}
              setWstCode={setWstCode}
              setWstStatus={setWstStatus}
              setDomainId={setDomainId}
              setLastName={setLastName}
              name={name}
              setStatus={setStatus}
              domainId={domainId}
              setFlagReg={setFlagReg}
              setFlagEditReg={setFlagEditReg}
              setPisUser={setPisUser}
              lastName={lastName}
              getPis={getPis}
              position={position}
              agency={agency}
              email={email}
              tel={tel}
              password={password}
              username={username}
              status={status}
              userGroup={userGroup}
              plantGroup={plantGroup}
              setMUserGroupId={setMUserGroupId}
              mUserGroupId={mUserGroupId}
              mPlantGroupId={mPlantGroupId}
              setMPlantGroupId={setMPlantGroupId}
              confPassword={confPassword}
              setConfPassword={setConfPassword}
              considerUserData={considerUserData}
              setCreateDate={setCreateDate}
              setUpdateDate={setUpdateDate}
              createDate={createDate}
              updateDate={updateDate}
              setCreateUser={setCreateUser}
              setUpdateUser={setUpdateUser}
              setFlagEditReg={setFlagEditReg}
              props={props}
            />
          )}
          {props.location.pathname ===
            SERVICE_IIS_DOOR + "consider_system/permission_management" && (
            <PermissionLogic setCroleArr={setCroleArr} props={props} setFlagPermission={setFlagPermission} />
          )}
        </main>
      </div>
    </div>
  );
};

export default ManagementLogic;
