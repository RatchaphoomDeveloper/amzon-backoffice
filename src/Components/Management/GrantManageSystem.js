import React from "react";
import Api from "../../Apis/Api";
import { SERVICE_IIS_DOOR } from "../../env";
import MessageStaus from "../../Status/message";
import DataTableindex from "../../Utils/dataTable";
import MainContent from "../Main/MainContent";
import "./mangement.scss";
const GrantManageSystem = (props) => {
  const [q, setQ] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  const [permissionUserData, setPermissionUserData] = React.useState([]);
  const replaceHeader = [
    "Id",
    "กลุ่มผู้ใช้งาน",
    "รับ E-mail",
    "สถานะ",
    "CODE_STATUS",
  ];
  function search(rows) {
    const columns = rows[0] && Object.keys(rows[0]);
    return rows.filter((row) =>
      columns.some(
        (column) =>
          row[column].toString().toLowerCase().indexOf(q.toLowerCase()) > -1
      )
    );
  }

  const getPermissionUserGroup = async () => {
    try {
      MessageStaus.showProgress()
      const permissionData = [];
      const response = await Api.getPermissionUserGroup();
      if (response) {
        response &&
          response.permissionUserDatas.forEach((res, i) => {
            permissionData.push({
              ID:res.id === null ? "" : res.id,
              NAME: res.name === null ? "" : res.name,
              EMAIL_REGISTER: res.emaiL_REGISTER === null ? "" : res.emaiL_REGISTER,
              status: res.status === null ? "" : res.status, 
              CODE_STATUS: res.codE_STATUS === null ? "" : res.codE_STATUS,
            });
          });
        setPermissionUserData(permissionData);
        MessageStaus.closeProgress()
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  React.useEffect(() => {
    getPermissionUserGroup();
  }, []);

 
  return (
    <div>
      <div className="main-content">
        <MainContent />
        <main>
          <div className="title-container">
            <h2 className="dash-tittle">
              {props.location.pathname === SERVICE_IIS_DOOR+ "grant_mange_system" &&
                "Managment/กำหนดสิทธิ์การใช้งาน"}
            </h2>
            <button type="button" className="md" onClick={()=>{
              setPermissionUserData([])
              getPermissionUserGroup()
            }} >
              <span className="fas fa-sync"></span>
              <label>รีเฟรช</label>
            </button>
          </div>

          <div className="serch-container">
            <span className="fas fa-search"></span>
            <input
              type="search"
              value={q}
              onChange={(e) => setQ(e.target.value)}
            />
          </div>

          <DataTableindex
            data={search(permissionUserData)}
            type="edit"
            linkTo={"/consider_system/permission_management?M_MENU_GROUP_ID="}
            replaceHeader={replaceHeader}
          />
        </main>
      </div>
   
    </div>
  );
};

export default GrantManageSystem;
