import { Component } from "react";
import axios from "axios";
import MessageStaus from "../Status/message";
class Api extends Component {
  static getPost = async () => {
    const result = await axios({
      url: "/posts",
      method: "get",
    }).catch((err) => {
      // MessageStaus.errresmessage(err.response.data.Message);
    });
    return result.data;
  };
  static getComments = async () => {
    const result = await axios({
      url: "/comments",
      method: "get",
    }).catch((err) => {
      // MessageStaus.errresmessage(err.response.data.Message);
    });
    return result.data;
  };
  static Login = async (data) => {
    const result = await axios({
      url: "/LoginLDAP",
      method: "post",
      data: data,
    });

    return result.data;
  };

  static checkLogin = async (data) => {
    const result = await axios({
      url: "/GetUser",
      method: "post",
      data: data,
    });
    return result.data;
  };

  static getLogLogin = async () => {
    const result = await axios({
      url: "/GetLogLogin",
      method: "get",
    });
    return result.data;
  };

  static saveLoginLog = async (data) => {
    const result = await axios({
      url: "/SaveLogLogin",
      method: "post",
      data: data,
    });
    return result.data;
  };

  static getConsiderUser = async () => {
    const result = await axios({
      url: "/GetConsiderUser",
      method: "get",
    });
    return result.data;
  };

  static getUserGroup = async () => {
    const result = await axios({
      url: "/UserGroup",
      method: "get",
    });
    return result.data;
  };

  static getPermissionUserGroup = async () => {
    const result = await axios({
      url: "/GetPermissionUserGroup",
      method: "get",
    });
    return result.data;
  };

  static saveUserPermissionGroup = async (data) => {
    const result = await axios({
      url: "/SaveUserPermissionGroup",
      method: "post",
      data: data,
    }).catch((err) => {
      MessageStaus.errresmessage(err.response.data.message);
    });
    return result.data;
  };

  static updateUserPermissionGroup = async (data) => {
    const result = await axios({
      url: "/UpdateUserPermissionGroup",
      method: "post",
      data: data,
    }).catch((err) => {
      MessageStaus.errresmessage(err.response.data.message);
    });
    return result.data;
  };

  static getMenus = async () => {
    const result = await axios({
      url: "/GetMMenu",
      method: "get",
    });
    return result.data;
  };

  static saveCRole = async (data) => {
    const result = await axios({
      url: "/SaveCRole",
      method: "post",
      data: data,
    }).catch((err) => {
      MessageStaus.errresmessage(err.response.data.message);
    });
    return result.data;
  };

  static sentCRole = async (data) => {
    const result = await axios({
      url: "/GetCRole",
      method: "post",
      data: data && data,
    }).catch((err) => {
    });
    return result.data;
  };

  static sendEmail =async(data)=>{
    const result = await axios({
      url: "/SendEmail",
      method: "post",
      data: data && data,
    }).catch((err) => {
    });
    return result.data;
  }
  static sentCRole = async (data) => {
    const result = await axios({
      url: "/GetCRole",
      method: "post",
      data: data && data,
    }).catch((err) => {
    });
    return result.data;
  };

  static Register = async (data) => {
    const result = await axios({
      url: "/Register",
      method: "post",
      data: data,
    }).catch((err) => {
      MessageStaus.errresmessage("ทำรายการไม่สำเร็จ");
    });
    return result.data;
  };

  static getDomain =async()=>{
    const result = await axios({
      url: "/Domain",
      method: "get",
    });
    return result.data;
  }

  static getUser =async(data)=>{
    const result = await axios({
      url: "/GetUser",
      method: "get",
      data: data,
    })
    return result.data
  }

  static getPis =async(data)=>{
    const result = await axios({
      url: "/GetUserPIS",
      method: "post",
      data: data,
    })
    return result.data
  }
  static getUser =async(data)=>{
    const result = await axios({
      url: "/GetUser",
      method: "post",
      data: data,
    })
    return result.data
  }
  
  static changeStatus=async(data)=>{
    const result = await axios({
      url: "/SetUserStatus",
      method: "post",
      data: data,
    })
    return result.data
  }

  static approveStatus=async(data)=>{
    const result = await axios({
      url: "/ApproveUser",
      method: "post",
      data: data,
    })
    return result.data
  }

  static getVersion=async()=>{
    const result = await axios({
      url: "/GetVersion?type=m",
      method: "get",
    })
    return result.data
  }

  static getPlant=async()=>{
    const result = await axios({
      url: "/Plant",
      method: "get",
    })
    return result.data
  }

  static getAllFiles=async()=>{
    const result = await axios({
      url: "/GetAllFiles?File_Type=APK",
      method: "get",
    })
    return result.data
  }

  static getAllManFiles=async()=>{
    const result = await axios({
      url: "/GetAllFiles?File_Type=MNL",
      method: "get",
    })
    return result.data
  }

  static updateFileStatus=async(data)=>{
    const result = await axios({
      url: "/UpdateFileStatus",
      method: "post",
      data: data,
    })
    return result.data
  }

  static editUSer=async(data)=>{
    const result = await axios({
      url: "/EditUserDetail",
      method: "post",
      data: data,
    })
    return result.data
  }

  static changePassword=async(data)=>{
    const result = await axios({
      url: "/ChangePassword",
      method: "post",
      data: data,
    })
    return result.data
  }

  static forgotPassword=async(data)=>{
    const result = await axios({
      url: "/ForgetPassword",
      method: "post",
      data: data,
    })
    return result.data
  }
  static getLogServices=async(data)=>{
    const result = await axios({
      url: "/GetLogService?Log_type="+data,
      method: "get",
    })
    return result.data
  }



  static saveApkFile=async(formdata,fileName,verSion,status,full_link,fileType)=>{
    const result = await axios({
      url: `/SaveApkFile?FileName=${fileName}&Version=${verSion}&Status=${status}&Full_Link=${full_link}&FileType=${fileType}`,
      method: "post",
      data: formdata,
    }).catch((err) => {
      MessageStaus.errresmessage("ทำรายการไม่สำเร็จ");
    });
    return result.data
  }


}

export default Api;
