import React, { useEffect } from "react";
import Api from "../../Apis/Api";
import { SERVICE_IIS_DOOR } from "../../env";
import DataTableindex from "../../Utils/dataTable";
import MainContent from "../Main/MainContent";
import "./LogTransection.scss";
const LogTransection = () => {
  const [data, setData] = React.useState([]);
  const [q, setQ] = React.useState("");
  const [loading, setLoading] = React.useState(false);

  function search(rows) {
    const columns = rows[0] && Object.keys(rows[0]);
    return rows.filter((row) =>
      columns.some(
        (column) =>
          row[column].toString().toLowerCase().indexOf(q.toLowerCase()) > -1
      )
    );
  }
  const fetchPosts = async () => {
    try {
      const response = await Api.getComments();
      setData(response);
    } catch (err) {}
  };
  useEffect(() => {
    fetchPosts();
    setLoading(false);
    return () => {};
  }, []);
  return (
    <div>
      <div className="main-content">
        <MainContent />
        <main>
          <div className="title-container">
            <h2 className="dash-tittle">
              {window.location.pathname === "/logs_transection" &&
                "Transiton Logs"}
            </h2>
            <button type="button" className="md">
              <span className="fas fa-sync"></span>
              <label>รีเฟรช</label>
            </button>
          </div>

          <div className="serch-container">
            <span className="fas fa-search"></span>
            <input
              type="search"
              value={q}
              onChange={(e) => setQ(e.target.value)}
            />
          </div>
          {/* {data[0] && <DataTableindex data={search(data)} />} */}
        </main>
      </div>
    </div>
  );
};

export default LogTransection;
