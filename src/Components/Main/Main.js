import React from "react";
import { NavLink } from "react-router-dom";
import { SERVICE_IIS_DOOR } from "../../env";
import MainContent from "./MainContent";
import "./Mian.scss";
const Main = () => {
  return (
    <div>
      <div className="main-content">
        <MainContent />
        <main>
          <h2 className="dash-tittle">
            {window.location.pathname === SERVICE_IIS_DOOR && "Home"}
          </h2>
          <div className="dash-cards">
            <div className="card-single">
              <div className="card-body">
                <span className="fas fa-briefcase"></span>
                <div>
                  <h5>EARNINGS(MONNTHLY)</h5>
                  <h4>$30,659.44</h4>
                </div>
              </div>
              <div className="card-footer">
                <NavLink to="/">View all</NavLink>
              </div>
            </div>

            <div className="card-single">
              <div className="card-body">
                <span className="fas fa-briefcase"></span>
                <div>
                  <h5>EARNINGS(ANNUAL)</h5>
                  <h4>$30,659.44</h4>
                </div>
              </div>
              <div className="card-footer">
                <NavLink to="/">View all</NavLink>
              </div>
            </div>

            <div className="card-single">
              <div className="card-body">
                <span className="fa fa-th-list"></span>
                <div>
                  <h5>TASKS</h5>
                  <h4>50%</h4>
                </div>
              </div>
              <div className="card-footer">
                <NavLink to="/">View all</NavLink>
              </div>
            </div>

            <div className="card-single">
              <div className="card-body">
                <span className="fas fa-sync"></span>
                <div>
                  <h5>PENDING REQUESTS</h5>
                  <h4>18</h4>
                </div>
              </div>
              <div className="card-footer">
                <NavLink to="/">View all</NavLink>
              </div>
            </div>
          </div>
        </main>
      </div>

     
    </div>
  );
};

export default Main;
