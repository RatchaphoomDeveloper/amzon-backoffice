import React from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import "./SideBar.scss";
import { SERVICE_IIS_DOOR } from "../../env";
import MainMobile from "../Main/MainMobile";
const SideBar = () => {
  const dispatch = useDispatch();
  const handleLogOut = () => {
    dispatch({ type: "set_user", user: {} });
  };
  return (
    <div>
      <div className="App-mobile">
        <MainMobile />
      </div>
      <div className="sidebar">
        <div className="sidebar-header">
          <div className="brand">
            <img
              src={process.env.PUBLIC_URL + "/images/logo.png"}
              onClick={handleLogOut}
            />
          </div>

          <span className="fa fa-bars"></span>
        </div>
        <div className="sidebar-menu">
          <div className="">
            <ul>
              <li>
                <Link to={"/"}>
                  <span className="fas fa-home"></span>
                  <span>Home</span>
                </Link>
              </li>
            </ul>
            <div className="main-side"></div>
          </div>
          <div className="foot-side">
            <p id="menu-p">INTERFACE</p>
            <ul>
              <li>
                <Link to={"/logs_login"}>
                  <span className="fas fa-wrench"></span>
                  <span>Log Login</span>
                </Link>
              </li>
            </ul>
            <div className="">
              <ul>
                <li>
                  <label for="btn-1" className="text-menu">
                    <div>
                      <span id="icon-sub" className="fas  fa-wrench"></span>
                      <span>Log Transection</span>
                    </div>
                    <span id="right" className="fas fa-chevron-right"></span>
                  </label>
                  <input type="checkbox" id="btn-1" />
                  <ul>
                    <li>
                      <Link to={"/log_saps"}>
                        <span id="sub-menu">Log Sap</span>
                      </Link>
                    </li>
                    <li>
                      <Link to={"/log_webservices"}>
                        <span id="sub-menu">Log Webservice</span>
                      </Link>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
            <div className="main-side"></div>
          </div>

          {/* <div className="detail-side">
            <p id="menu-p">INTERFACE</p>
            <ul>
              <li>
                <Link to={"/logs_login"}>
                  <span className="fas fa-wrench"></span>
                  <span>Log Login</span>
                </Link>
              </li>
              <li>
                <Link to={"/logs_transection"}>
                  <span className="fas  fa-wrench"></span>
                  <span>Log Transection</span>
                </Link>
              </li>
            </ul>
            <div className="main-side"></div>
          </div> */}
          <div className="foot-side">
            <p id="menu-p">MANAGEMENT</p>
            <ul>
              <li>
                <Link to={"/consider_system"}>
                  <span className="fas fa-cog"></span>
                  <span>พิจารณาผู้มีสิทธิใช้งานระบบ</span>
                </Link>
              </li>
              <li>
                <Link to={"/grantuser_system"}>
                  <span className="fas fa-cog"></span>
                  <span>ผู้มีสิทธิใช้งานระบบ</span>
                </Link>
              </li>
              <li>
                <Link to={"/grant_mange_system"}>
                  <span className="fas fa-cog"></span>
                  <span>กำหนดสิทธิ์การใช้งาน</span>
                </Link>
              </li>
              <li>
                <Link to={"/grant_user_group_system"}>
                  <span className="fas fa-cog"></span>
                  <span>กำหนดกลุ่มผู้ใช้งาน</span>
                </Link>
              </li>
              <li>
                <Link to={"/change_password"}>
                  <span className="fas fa-cog"></span>
                  <span>Change Password</span>
                </Link>
              </li>
            </ul>
            <div className="main-side"></div>
          </div>
          <div className="foot-side">
            <p id="menu-p">APP MANAGEMENT</p>
            <li>
              <Link to={"/appinstall_management"}>
                <span class="fab fa-android"></span>
                <span>App Installer</span>
              </Link>
            </li>
            <li>
              <Link to={"/manaul_management"}>
                <sapn class="fas fa-file-word"></sapn>
                <span>Upload Manual</span>
              </Link>
            </li>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SideBar;
