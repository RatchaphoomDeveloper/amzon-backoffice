import React from "react";
import Api from "../../Apis/Api";
import { SERVICE_IIS_DOOR } from "../../env";
import DataTableindex from "../../Utils/dataTable";
import MainContent from "../Main/MainContent";
import moment from "moment";
import "./mangement.scss";
import ValidateStaus from "../../Status/validate";
import MessageStaus from "../../Status/message";
import { INPUT_ERROR, INPUT_LENGTH_WRONG } from "../../Status/status";
import { useSelector } from "react-redux";
import {
  BrowserView,
  MobileView,
  isBrowser,
  isMobile
} from "react-device-detect";
const GrantGroupSystem = (props) => {
  const [q, setQ] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  const [id, setId] = React.useState("");
  const [permissionUserData, setPermissionUserData] = React.useState([]);
  const [permissionUserAllData, setPermissionUserAllData] = React.useState([]);
  const [permissionFilterData, setPermissionFilterData] = React.useState({});
  const [insertFlag, setInsertFlag] = React.useState(true);
  const replaceHeader = [
    "Id",
    "กลุ่มผู้ใช้งาน",
    "หมายเหตุ",
    "สถานะ",
    "CODE_STATUS",
  ];
  const [groupName, setGroupName] = React.useState("");
  const [message, setMessage] = React.useState("");
  const [status, setStatus] = React.useState(null);
  const [createDate, setCreateDate] = React.useState("");
  const [createBy, setCreateBy] = React.useState("");
  const [updateDate, setUpdateDate] = React.useState("");
  const [updateBy, setUpdateBy] = React.useState("");
  const userData = useSelector((state) => state.user);
  function search(rows) {
    const columns = rows[0] && Object.keys(rows[0]);
    return rows.filter((row) =>
      columns.some(
        (column) =>
          row[column].toString().toLowerCase().indexOf(q.toLowerCase()) > -1
      )
    );
  }

  const clearState = () => {
    setGroupName("");
    setMessage("");
    setStatus(null);
    setCreateDate("");
    setCreateBy("");
    setUpdateDate("");
    setUpdateBy("");
  };

  const filterGroup = () => {
    try {
      if (id !== "") {
        const dataFilter =
          permissionUserAllData &&
          permissionUserAllData.filter((res, i) => {
            return res.id === id;
          });

        setPermissionFilterData(dataFilter[0]);
        // setMessage(dataFilter[0] && permissionFilterData.message);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  React.useEffect(() => {
    filterGroup();
    clearState();
  }, [id]);

  React.useEffect(() => {
    setStatus(permissionFilterData.status);
    setMessage(permissionFilterData.message);
  }, [permissionFilterData]);

  const updateUserPermissionGroup = async () => {
    try {
      const data = {
        id: permissionFilterData.id,
        message: message !== "" ? message : permissionFilterData.message,
        status:
          status !== null
            ? status === "ไม่ใช้งาน"
              ? "0"
              : "1"
            : permissionFilterData.status === "ใช้งาน"
            ? "1"
            : "0",
        m_USER_ID_CREATE: userData.id,
      };
      if (data.id === undefined) {
        return MessageStaus.errresmessage("ไม่พบข้อมูล group id");
      }
      const response = await Api.updateUserPermissionGroup(data);
      if (response) {
        setTimeout(()=>{
          MessageStaus.successmessage(response.message);
        },500)
        getPermissionUserGroup();

      } else {
        setTimeout(()=>{
          MessageStaus.errresmessage(response.message);
        },500)
        getPermissionUserGroup();
      }
    } catch (error) {}
  };

  const handleSubmit = async () => {
    try {
      const userPData = {
        id: "",
        code: groupName,
        name: groupName,
        message: message,
        status: status === null ? "1" : status.toString(),
        codE_STATUS: "",
        emaiL_REGISTER: "0",
        datE_CREATE: moment().format("DD/MM/YYYY hh:mm:ss"),
        datE_UPDATE: moment().format("DD/MM/YYYY hh:mm:ss"),
        m_USER_ID_CREATE: userData.id,
      };

      if (groupName === "") {
        return MessageStaus.messages(INPUT_ERROR);
      }
      if (status === "" || status === null) {
        return MessageStaus.errresmessage("กรุณาเลือกสถานะก่อนทำการแก้ไข");
      }

      if (groupName !== "" && groupName.length <= 10) {
        const response = await Api.saveUserPermissionGroup(userPData);
        if (response) {
          setTimeout(()=>{
            MessageStaus.successmessage(response.message);
          },500)
          clearState();
          getPermissionUserGroup();
        }
      } else {
        MessageStaus.errresmessage(
          "ชื่อกลุ่มผู้ใช้งานนี้ข้อมูลแล้วในระบบ กรุณากรอกใหม่อีกครั้ง"
        );
      }
    } catch (err) {
      return MessageStaus.errresmessage(
        "ชื่อกลุ่มผู้ใช้งานนี้ข้อมูลแล้วในระบบ หรือ กรอกข้อมูลไม่ถูกต้อง กรุณากรอกใหม่อีกครั้ง"
      );
    }
  };

  const getPermissionUserGroup = async () => {
    try {
      MessageStaus.showProgress()
      const permissionData = [];
      const response = await Api.getPermissionUserGroup();
      if (response) {
        response &&
          response.permissionUserDatas.forEach((res) => {
            permissionData.push({
              ID: res.id,
              NAME: res.name,
              EMAIL_REGISTER: res.message,
              status: res.status,
              CODE_STATUS: res.codE_STATUS,
            });
          });
        setPermissionUserData(permissionData);
        setPermissionUserAllData(response.permissionUserDatas);
        MessageStaus.closeProgress()
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  React.useEffect(() => {
    getPermissionUserGroup();
    setInsertFlag(false);
  }, []);

  return (
    <div>
      
      <div className="main-content">
        <MainContent />
        <main>
          <div className="title-container">
            <h2 className="dash-tittle">
              {props.location.pathname ===
                SERVICE_IIS_DOOR + "grant_user_group_system" &&
                "Managment/กำหนดกลุ่มผู้ใช้งาน"}
            </h2>
            <div className="logic-but-content">
              {insertFlag === true ? (
                <div>
                  <button
                    type="button"
                    className="md"
                    id="button-submit"
                    onClick={handleSubmit}
                  >
                    <label>บันทึก</label>
                  </button>
                  <button
                    type="button"
                    className="md"
                    onClick={() => {
                      setInsertFlag(false);
                      setId("");
                      clearState();
                    }}
                  >
                    <label>ปิด</label>
                  </button>
                </div>
              ) : (
                <div>
                  <button
                    type="button"
                    className="md"
                    onClick={() => {
                      setPermissionUserData([]);
                      setPermissionUserAllData([]);
                      getPermissionUserGroup();
                      clearState();
                    }}
                  >
                    <span className="fas fa-sync"></span>
                    <label>รีเฟรช</label>
                  </button>
                  {id !== "" && <button
                  type="button"
                  className="md"
                  id="button-submit"
                  onClick={updateUserPermissionGroup}
                >
                  <label>บันทึก</label>
                </button> }
                  <button
                    type="button"
                    className="md"
                    onClick={() => {
                      setInsertFlag(true);
                      setId("");
                      clearState();
                    }}
                  >
                    <span className="fas fa-plus-circle"></span>
                    <label>เพิ่ม</label>
                  </button>
                </div>
              )}
            </div>
          </div>
          <div className="table-card-container">
            <div className="table-single-card">
              <div className="serch-container">
                <span className="fas fa-search"></span>
                <input
                  type="search"
                  value={q}
                  onChange={(e) => setQ(e.target.value)}
                />
              </div>
              <DataTableindex
                data={search(permissionUserData)}
                type="edit"
                clicked={true}
                linkTo={"/consider_system/permission_management"}
                setId={setId}
                replaceHeader={replaceHeader}
              />
            </div>

            {insertFlag === true ? (
              <div>
                <div>
                  {permissionFilterData && (
                    <div
                      className="table-single-card-pad-form"
                      style={{ paddingBottom: "200px" }}
                    >
                      <div className="table-grant-group-form">
                        <div className="table-grant-group-input">
                          <label>ชื่อกลุ่ม:</label>
                          <br />
                          <input
                            type="text"
                            value={groupName}
                            onChange={(e) => setGroupName(e.target.value)}
                          />
                        </div>
                      </div>
                      <div className="table-grant-group-form">
                        <div className="table-grant-group-input">
                          <label>หมายเหตุ:</label>
                          <br />
                          <input
                            type="text"
                            value={message}
                            onChange={(e) => setMessage(e.target.value)}
                          />
                        </div>
                      </div>
                      <p style={{ paddingLeft: "1rem" }}>สถานะ :</p>
                      <div className="table-grant-checkbox-container-inline">
                        <div className="table-grant-radio-form-group">
                          <input
                            type="radio"
                            id="use"
                            name="status"
                            value={1}
                            onChange={(e) => {
                              setStatus(e.target.value);
                            }}
                          />
                          <label for="male">ใช้งาน</label>
                        </div>
                        <div className="table-grant-radio-form-group">
                          <input
                            type="radio"
                            id="disable"
                            name="status"
                            value={0}
                            onChange={(e) => setStatus(e.target.value)}
                          />
                          <label for="female">ไม่ใช้งาน</label>
                        </div>
                      </div>
                      <div className="table-general-form">
                        <div className="general-input">
                          <label>Create on</label>
                          <br />
                          <input
                            type="text"
                            value={userData.username}
                            disabled
                            onChange={(e) => setCreateBy(e.target.value)}
                          />
                        </div>
                        <div className="general-input">
                          <label>Create by</label>
                          <br />
                          <input
                            type="text"
                            value={moment()
                              .format("DD/MM/YYYY hh:mm:ss")
                              .toString()}
                            onChange={(e) => setCreateDate(e.target.value)}
                            disabled
                          />
                        </div>
                      </div>
                      <div className="table-general-form">
                        <div className="general-input">
                          <label>Update on</label>
                          <br />
                          <input
                            type="text"
                            value={userData.username}
                            disabled
                            onChange={(e) => setUpdateBy(e.target.value)}
                          />
                        </div>
                        <div className="general-input">
                          <label>Update by</label>
                          <br />
                          <input
                            type="text"
                            value={moment()
                              .format("DD/MM/YYYY hh:mm:ss")
                              .toString()}
                            onChange={(e) => setUpdateDate(e.target.value)}
                            disabled
                          />
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            ) : (
              <div>
                {id === "" ? (
                  <div className="table-single-card-pad">
                    <label>กดเลือกข้อมูลที่ต้องการแก้ไข</label>
                  </div>
                ) : (
                  <div>
                    <div
                      className="table-single-card-pad-form"
                      style={{ paddingBottom: "200px" }}
                    >
                      <div className="table-grant-group-form">
                        <div className="table-grant-group-input">
                          <label>ชื่อกลุ่ม:</label>
                          <br />
                          <input
                            type="text"
                            value={permissionFilterData.name}
                            disabled
                          />
                        </div>
                      </div>
                      <div className="table-grant-group-form">
                        <div className="table-grant-group-input">
                          <label>หมายเหตุ:</label>
                          <br />
                          <input
                            type="text"
                            value={message && message}
                            onChange={(e) => setMessage(e.target.value)}
                          />
                        </div>
                      </div>
                      <p style={{ paddingLeft: "1rem" }}>สถานะ :</p>
                      <div className="table-grant-checkbox-container-inline">
                        <div className="table-grant-radio-form-group">
                          <input
                            type="radio"
                            value={"ใช้งาน"}
                            name="status"
                            checked={status === "ใช้งาน"}
                            onChange={(e) => setStatus(e.target.value)}
                          />

                          <label for="male">ใช้งาน</label>
                        </div>
                        <div className="table-grant-radio-form-group">
                          <input
                            type="radio"
                            value={"ไม่ใช้งาน"}
                            name="status"
                            checked={status === "ไม่ใช้งาน"}
                            onChange={(e) => setStatus(e.target.value)}
                          />
                          <label for="female">ไม่ใช้งาน</label>
                        </div>
                      </div>
                      <div className="table-general-form">
                        <div className="general-input">
                          <label>Create on</label>
                          <br />
                          <input
                            type="text"
                            disabled
                            value={permissionFilterData.datE_CREATE}
                          />
                        </div>
                        <div className="general-input">
                          <label>Create by</label>
                          <br />
                          <input
                            type="text"
                            disabled
                            value={
                              permissionFilterData.m_USER_ID_CREATE === null
                                ? " "
                                : permissionFilterData.m_USER_ID_CREATE
                            }
                          />
                        </div>
                      </div>
                      <div className="table-general-form">
                        <div className="general-input">
                          <label>Update on</label>
                          <br />
                          <input
                            type="text"
                            disabled
                            value={permissionFilterData.datE_UPDATE}
                          />
                        </div>
                        <div className="general-input">
                          <label>Update by</label>
                          <br />
                          <input
                            type="text"
                            disabled
                            value={permissionFilterData.m_USER_ID_UPDATE}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            )}
          </div>
        </main>
      </div>

                          
    </div>
  );
};

export default GrantGroupSystem;
