import React from "react";
import './Footer.scss'
const Footer = () => {
  return (
    <div>
      <footer>
        <div className="footer-detail">
          <h5>PTT Digital | Version 20201215</h5>
        </div>
      </footer>
    </div>
  );
};

export default Footer;
